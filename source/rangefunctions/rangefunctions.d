module rangefunctions.rangefunctions;
import std.range : isForwardRange, empty, front, popFront, save, ElementType, drop, repeat, chain, join, repeat, take;
import std.algorithm : map, min;
import std.traits;
import std.typecons : tuple;
import containers.circularbuffer;

struct SlideToEnd(R) {
  R r;
  auto front() { return r.save; }
  auto popFront() { r.popFront; }
  auto save() { return typeof(this)(r.save); }
  auto empty() { return r.empty; }
  static if(__traits(compiles,R.init.length) && __traits(compiles,R.init[0..$])) {
    auto length() { return r.length; }
    auto opIndex(long i) { return r.save[i..$]; }
    auto opSlice(long i, long j) { return slideToEnd(r.save[i..$]).take(j-i); }
    auto opDolar() { return length; }
  }
}

auto slideToEnd(R)(R r) { return SlideToEnd!R(r); }
unittest{
  import std.algorithm : equal;
  auto r = [1,2,3,4,5];
  assert(r.slideToEnd.equal!equal([[1,2,3,4,5],[2,3,4,5],[3,4,5],[4,5],[5]]));
}

struct SubsetRet(size_t k, R) {
  alias T = ElementType!R;
  R r;
  static if(k>0) {
    T x;
    SubsetRet!(k-1,R) s;
    bool _empty = false;
    auto empty() { return _empty || s.empty; }
    auto front() {
      return only(x).chain(s.front);
    }
    void popFront() {
      assert(!_empty && !s.empty);
      s.popFront;
      if(s.empty) {
         if(!r.empty) { prep(); }
         else {_empty = true; }
      }
    }
    auto save() {
      return typeof(this)(r.save,x,s.save);
    }
    void prep() {
      if(!r.empty) {
        x = r.front;
        r.popFront;
        s = r.save.subsets!(k-1);
      } else {
        _empty = true;
      }
    }
  } else  {
   bool empty = false;
   auto front = R.init.take(0);
   void popFront() { empty = true; }
   auto save() {
     return typeof(this)(r,empty,front);
   }
   void prep() {}
  }
}
auto subsets(size_t k,R)(R r) {
  auto ret = SubsetRet!(k,R)(r);
  ret.prep();
  return ret;
}


struct IotaReturn(T,S) {
  T cur;
  T end;
  S step;
  @property auto front() { return cur; }
  @property auto empty() { return !(cur <  end); }
  void popFront() { cur = cur + step; }
  auto save() { return IotaReturn(cur,end,step); }
 }

auto myIota(T,S)(T start, T end, S step) 
if (__traits(compiles,start + step < end)){
 return IotaReturn!(T,S)(start,end,step);
}


struct SlideRet(R) {
 alias T = ElementType!R ;
 R r;
 ulong s;
 ulong step;
 bool partial;
 CircularBuffer!(T[]) x;
 bool empty;
 auto front() { return x.save; }
 void popFront() {
  x.popNFront(min(step,x.length));
  prime(); 
 }
 auto save() {
  return SlideRet!R(r.save,s,step,partial,x.save,empty);
 }
 void prime() {
  while(!r.empty && x.length < s) {
   x.pushBack(r.front);
   r.popFront();
  }
  empty = x.empty || (x.length < s && !partial);
 }
}

auto slide(R)(R r, ulong s,ulong step=1, bool partial=false) {
 alias T = ElementType!R ;
 auto ret= SlideRet!R(r,s,step,partial,makeCircularBuffer!(T[])(s));
 ret.prime;
 return ret;
}
unittest {
 import std.algorithm;
 [1,2].equal([1]);
 auto x = [1,2,3,4,5,6,7,8,9,10];
 auto s = x.slide(2);
 auto t = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7],[7,8],[8,9],[9,10]];
 assert(equal!equal(s,t));
 auto s1 = x.slide(9);
 auto t1 = [[1,2,3,4,5,6,7,8,9],[2,3,4,5,6,7,8,9,10]];
 assert(equal!equal(s1,t1));
 auto s2 = x.slide(10);
 auto t2 = [[1,2,3,4,5,6,7,8,9,10]];
 assert(equal!equal(s2,t2));
 auto s3 = x.slide(11);
 auto t3 = typeof(t2).init;
 assert(equal!equal(s3,t3));
 auto s4 = x.slide(2).slide(2);
 assert(equal!(equal!equal)(s4,[[[1,2],[2,3]],[[2,3],[3,4]],[[3,4],[4,5]],[[4,5],[5,6]],[[5,6],[6,7]],[[6,7],[7,8]],[[7,8],[8,9]],[[8,9],[9,10]]]));
 assert([1,2,3,4].slide(3,1,true).equal!equal([[1,2,3],[2,3,4],[3,4],[4]]));
 assert([1,2,3,4].slide(3,2,true).equal!equal([[1,2,3],[3,4]]));
}
auto tracedRange(R)(R r, string pref="") {
import std.stdio;
static struct Ret {
 R r;
 string pref;
 @property auto empty() { bool e = r.empty; std.stdio.writef("%sChecked if range was empty: %s\n",pref,e); return e; }
 @property auto front() {
  static if(isForwardRange!(ElementType!R)) {
   auto f = r.front.save;
  } else {
   auto f = r.front;
  }
  std.stdio.writef("%sGot front: %s\n",pref, f);
  return f; }
 void popFront() {
  std.stdio.writef("%sPopped front.",pref);
  r.popFront;
  std.stdio.writef("%sNow empty?%s\n",pref,r.empty);
 }
 auto save() { std.stdio.writefln("%sSaving",pref); return Ret(r.save,"."~pref); }
}
return Ret(r,pref);
}

auto rangeFuncIf(alias t, alias f)(bool x) {
return (true.repeat(x?1:0).map!t.join).chain(true.repeat(x?0:1).map!f.join);
}

unittest{
 import std.range : only;
 import std.stdio;
 int n = 2;
 auto r = (n>1).rangeFuncIf!(
  (b)=>only(1,2,3),
  (b)=>[4,5,6]);
 r.writeln;
}


///Basic circular buffer
struct CircBuffer(T) {
T[] data;
ushort frontIndex = 0;
ushort backIndex = 0;
ushort count = 0;
ushort capacity;
ushort maxCapacity;

this(ushort C) {
 data.length = C+1;
 capacity = C;
 maxCapacity = C;
}
this(T[] d, ushort fI, ushort bI, ushort cnt, ushort c) {
 data = d.dup;
 frontIndex = fI;
 backIndex = bI;
 cnt = cnt;
 capacity = c;
 maxCapacity = cast(ushort) (d.length - 1);
}
void clearBuffer(ushort cap) { /// clears the circular buffer and sets the capacity to cap (if cap <= maxCapacity)
 frontIndex = 0;
 backIndex = 0;
 count = 0;
 capacity = min(cap, maxCapacity);
}

void inc(ref ushort x) { x++; x%=(capacity+1); }
void dec(ref ushort x) { x+=capacity; x%=(capacity +1); }

@property bool empty() {return frontIndex == backIndex; }///
@property bool full() {return count == capacity; }///

bool append(T x) { ///
 data[backIndex] =  x;
 inc(backIndex);
 if (frontIndex == backIndex) {
  inc(frontIndex);
  return false;
 }
 count++;
 return true;
}

@property auto front() { return data[frontIndex]; }///
void popFront() {if (!empty()) { inc(frontIndex); count--; }}///

bool prepend(T x) { ///
 dec(frontIndex);
 data[frontIndex] = x;
 if (frontIndex == backIndex) {
  dec(backIndex);
  return false;
 }
 count ++;
 return true;
}

@property auto back() { auto x = backIndex; dec(x); return data[x]; }///
void popBack() { if (!empty()) { dec(backIndex); count--; } } ///

ushort toArrayIndex(ushort index) { return (frontIndex+index)%(capacity +1); }

@property T opIndex(ushort index) { return data[toArrayIndex(index)]; }///

auto save() { return CircBuffer(data, frontIndex, backIndex, count, capacity); }
}

/+++
Finds the moving maximum of r=[r0,r1,...,ri,...] according to ordering ord such
where the output range [x0,x1,...,xi,...] has xi the maximum of all rj such
that j <= i and close(rj,ri) is true. Assumes that if j < k < i and close(rk,ri) is false
then close(rj,ri) is false.
+++/
auto movingMax(alias close, alias ord, R)(R r, ushort s) {
alias T = ElementType!R;
struct Ret {
 R f;
 R b;
 T c;
 bool empty_;
 CircularBuffer!(T[]) maxes;
 @property bool empty() { return empty_; }
 @property auto front() { return T(c[0],maxes.front[1]); }
 void popFront() {
  
  //writef("In-->%s|%s|%s|%s<--\n",f,b,maxes,maxes.data);
  if (f.empty) { empty_ = true; return; }
  c = f.front;
  if (!close(b.front,f.front)) {
    if (b.front == maxes.front) { maxes.popFront; }
    b.popFront;
  }
  while(!maxes.empty && ord(f.front(), maxes.back())) { maxes.popBack; }
  if (maxes.full) {
   auto tmp = maxes;
   
  }
  maxes.append(f.front);
  f.popFront;
  //writef("Out-->%s|%s<--\n",maxes,maxes.data);
 }
 auto save() { return Ret(f.save,b.save,c,empty_,maxes.save); }
}
auto ret = Ret(r.save,r,T.init,false,makeCircularBuffer!(T[])(s));
ret.popFront;
return ret;
}
unittest{
 import std.range;
 import std.stdio;
 import std.algorithm;
 auto r = movingMax!((a,b)=>(a[0]<b[0]?b[0]-a[0]:a[0]-b[0])<3,(a,b)=>a[1]<b[1])(iota(10).enumerate,10);
 assert(r.map!(a=>a[1]).equal(only(0,0,0,1,2,3,4,5,6,7)));
}
///Moving average
auto movingAverage(alias close, R)(R r) {
static struct Ret {
 alias T = ElementType!R;
 R f;
 R b;
 T c;
 ulong cnt=0;
 bool empty_;
 @property empty() { return empty_; }
 @property front() { return T(c[0],c[1]/cnt); }
 void popFront() {
  if (f.empty) {empty_ = true; return; }
  c[0] = f.front[0]; c[1]+=f.front[1];
  cnt ++;
  if (!close(b.front,f.front)) {
   c[1]-=b.front[1];
   cnt --;
   b.popFront;
  }
  f.popFront;
 }
 auto save() { return Ret(f,b,c,cnt,empty_); }
}
return Ret(r.save.drop(1),r,r.front,1);
}

auto myInit(T)(ulong n) {
T[] ret;
ret.length = n;
return ret;
}

auto movingBuckets(alias close, R)(R r, double minV, double maxV, ulong numBuckets) {
import std.conv;
import std.typecons;

static struct MovingBuckets {
 ulong cnt;
 ulong[] buckets;
 ulong numBuckets;
 double minV;
 double maxV;
 R f;
 R l;
 bool _empty;
 auto bucketIndex(double v) { return (numBuckets*(v-minV)/(maxV-minV)).to!ulong; }
 auto indexToVal(ulong i) { return (maxV-minV)*i/numBuckets + minV; }
 @property front() { return tuple(buckets,cnt); }
 @property empty() { return _empty; }
 auto popFront() {
  if(l.empty) {_empty=true; return;}
  if (!close(f.front,l.front)) {
   buckets[bucketIndex(f.front[1])]--;
   cnt--;
   f.popFront;
  }
  buckets[bucketIndex(l.front[1])] ++;
  cnt++;
  l.popFront;
 }
 auto save() { return MovingBuckets(cnt,buckets.dup,numBuckets,minV,maxV,f.save,l.save,_empty); }
}
auto ret = MovingBuckets(0,myInit!(ulong)(numBuckets),numBuckets,minV,maxV,r.save,r.save,false);
ret.popFront;
return ret;
}
struct MySplitterOuter(R) {
private R current;
private R next;
ulong grpNum;
ulong cnt;
}

struct MySplitterInner(alias pred, R) {
R r;
MySplitterOuter!(R) *o;
ulong grpNum;
ulong cnt;
@property front() { return r.front; }
@property empty() { return r.empty || (cnt > 0 && pred(r.front)); }
void popFront() {
 r.popFront;
 cnt++;
 if (is(o) && o.grpNum == grpNum && o.cnt < cnt) {o.next = r.save; o.cnt = cnt; }
}
auto save() { return MySplitterInner(r.save, o, grpNum, cnt); } 
}

struct MySplitterResult(alias pred, R) {

MySplitterOuter!(R) o;
this(R r) { o = MySplitterOuter!(R)(r.save,r.save,0,0); }
@property auto front() { return MySplitterInner!(pred,R)(o.current.save,&o); }
@property auto empty() { return o.current.empty; }
void popFront() {
 while(!o.next.empty && (o.cnt ==  0 || !pred(o.next.front))) {
  o.next.popFront;
  o.cnt++; 
 }
 o.current = o.next.save;
 o.cnt = 0;
 o.grpNum++;
}
auto save() {
 return typeof(this)(o.current.save);
}
}

auto mySplit(alias pred, R)(R r)
if(isForwardRange!R && __traits(compiles,pred(ElementType!R.init))) {
 return MySplitterResult!(pred,R)(r);
}

struct MySplitterInner2(alias pred, R) {
R r;
MySplitterOuter!(R) *o;
bool _empty;
ulong grpNum;
ulong cnt;
@property front() { return r.front; }
@property empty() { return r.empty || (cnt > 0 && pred(r.front)) || _empty; }
void popFront() {
 import std.stdio;
 if (pred(r.front)) _empty=true;
 r.popFront;
 cnt++;
 if (is(o) && o.grpNum == grpNum && o.cnt < cnt) {o.next = r.save; o.cnt = cnt; }
}
auto save() { return MySplitterInner2(r.save, o, _empty, grpNum, cnt); } 
}

struct MySplitterResult2(alias pred, R) {

MySplitterOuter!(R) o;
this(R r) { o = MySplitterOuter!(R)(r.save,r.save,0,0); }
@property auto front() { return MySplitterInner2!(pred,R)(o.current.save,&o); }
@property auto empty() { return o.current.empty; }
void popFront() {
 if (o.cnt == 0 && pred(o.next.front))  {
  o.next.popFront;
  o.cnt++;
 }
 else {
  while(!o.next.empty && (o.cnt ==  0 || !pred(o.next.front))) {
   o.next.popFront;
   o.cnt++; 
  }
 }
 import std.stdio;
 o.current = o.next.save;
 o.cnt = 0;
 o.grpNum++;
}
auto save() {
 return typeof(this)(o.current.save);
}
}

auto mySplit2(alias pred, R)(R r)
if(isForwardRange!R && __traits(compiles,pred(ElementType!R.init))) {
 return MySplitterResult2!(pred,R)(r);
}

struct ChunkByOuter(R) {
private R current;
private R next;
ElementType!R prev;
ulong grpNum;
ulong cnt;
}

struct ChunkByInner(alias pred, R) {
R r;
ElementType!R prev;
ChunkByOuter!(R) *o;
ulong grpNum;
ulong cnt;
@property front() { return r.front; }
@property empty() { return r.empty || !pred(prev,r.front); }
void popFront() {
 prev = r.front;
 r.popFront;
 cnt++;
 if(o!=null && o.grpNum == grpNum && o.cnt < cnt) {o.next = r.save; o.prev = prev; o.cnt = cnt; }
}
auto save() { return ChunkByInner(r.save,prev,o,grpNum,cnt); }
}

struct ChunkByResult(alias pred, R) {
ChunkByOuter!(R) o;
this(R r) { auto f = (r.empty?ElementType!R.init:r.front); o = ChunkByOuter!(R)(r.save,r.save,f,0,0); }
this(R current, R next, ElementType!R prev, ulong grpNum, ulong cnt) { o = ChunkByOuter!(R)(current.save,next.save,prev,grpNum,cnt); }
@property auto front() { return ChunkByInner!(pred, R)(o.current.save, o.current.front, &o,o.grpNum); }
@property auto empty() { return o.current.empty; }
void popFront() {
 while(!o.next.empty && pred(o.prev,o.next.front)) {
  o.prev = o.next.front;
  o.next.popFront;
 }
 if (!o.next.empty) {  o.prev = o.next.front; }
 o.current = o.next.save;
 o.cnt = 0;
 o.grpNum++;
}
auto save() {
 return typeof(this)(o.current.save, o.next.save, o.prev, o.grpNum, o.cnt);
}
}

auto myChunkBy(alias pred, R)(R r) 
if(isForwardRange!R && __traits(compiles,pred(ElementType!R.init,ElementType!R.init))) {
 return ChunkByResult!(pred,R)(r);
}

auto myChunkBy(alias pred, R) (R r)
if(isForwardRange!R && __traits(compiles,pred(ElementType!R.init))) {
 import std.typecons : tuple;
 return myChunkBy!((a,b)=>pred(a)==pred(b), R)(r).map!(x=>tuple(pred(x.front),x));
}

struct MyMapResult(alias f, R) {
R r;
@property auto empty() {return r.empty;}
@property auto front() {return f(r.front);}
void popFront() { return r.popFront; }
auto save() { return MyMapResult(r); }
}

auto myMap(alias f, R) (R r) {
return MyMapResult!(f,R)(r);
}

auto interpolate(R1, R2)(R1 xs, R2 points) {
import std.stdio;
static void fix(ref R1 xs, ref R2 cur, ref R2 next) {
 while(!next.empty && xs.front > next.front[0]) {
  cur.popFront;
  next.popFront;
 }
}
static struct Ret {
 R1 xs;
 R2 cur;
 R2 next;
 @property auto front() {
  import std.algorithm : max;
  auto t = max((xs.front - cur.front[0])/(next.front[0]-cur.front[0]),0);
  return tuple(xs.front, (1-t)*cur.front[1] + t*next.front[1]);
 }
 @property auto empty() { return next.empty || xs.empty; }
 void popFront() {
  xs.popFront();
  if (! xs.empty) { fix(xs,cur,next); }
 }
 auto save() { return Ret(xs.save,cur.save,next.save); }
}
auto cur = points;
auto next = points.save;
if (cur.empty) { return Ret(xs,cur,next); }
next.popFront;
fix(xs,cur,next);
return Ret(xs,cur,next);
}

auto allOrNothing(R)(R r, bool b) {
static struct Ret {
 R r;
 bool _empty;
 @property auto front() { return r.front; }
 @property empty() { return r.empty || _empty; }
 void popFront() { r.popFront; }
 auto save() { return Ret(r.save, _empty); }
}
return Ret(r.save,!b);
}

@system unittest
{
 import std.algorithm.comparison : equal;

 // Grouping by particular attribute of each element:
 auto data = [
  [1, 1],
  [1, 2],
  [2, 2],
  [2, 3]
 ];

  auto r1 = data.myChunkBy!((a,b) => a[0] == b[0]);
  assert(r1.equal!equal([
     [[1, 1], [1, 2]],
     [[2, 2], [2, 3]]
  ]));

  auto r2 = data.myChunkBy!((a,b) => a[1] == b[1]);
  assert(r2.equal!equal([
     [[1, 1]],
     [[1, 2], [2, 2]],
     [[2, 3]]
  ]));
}

version (none) // this example requires support for non-equivalence relations
 @safe unittest
{
 // Grouping by maximum adjacent difference:
 import std.math : abs;
 auto r3 = [1, 3, 2, 5, 4, 9, 10].myChunkBy!((a, b) => abs(a-b) < 3);
 assert(r3.equal!equal([
    [1, 3, 2],
    [5, 4],
    [9, 10]
 ]));

}

/// Showing usage with unary predicate:
/* FIXME: pure @safe nothrow*/ @system unittest
{
 import std.algorithm.comparison : equal;
 import std.range.primitives;
 import std.typecons : tuple;

 // Grouping by particular attribute of each element:
 auto range =
  [
  [1, 1],
  [1, 1],
  [1, 2],
  [2, 2],
  [2, 3],
  [2, 3],
  [3, 3]
  ];

  auto byX = myChunkBy!(a => a[0])(range);
  auto expected1 =
   [
   tuple(1, [[1, 1], [1, 1], [1, 2]]),
   tuple(2, [[2, 2], [2, 3], [2, 3]]),
   tuple(3, [[3, 3]])
   ];
  foreach (e; byX)
  {
   assert(!expected1.empty);
   assert(e[0] == expected1.front[0]);
   assert(e[1].equal(expected1.front[1]));
   expected1.popFront();
  }

  auto byY = myChunkBy!(a => a[1])(range);
  auto expected2 =
   [
   tuple(1, [[1, 1], [1, 1]]),
   tuple(2, [[1, 2], [2, 2]]),
   tuple(3, [[2, 3], [2, 3], [3, 3]])
   ];
  foreach (e; byY)
  {
   assert(!expected2.empty);
   assert(e[0] == expected2.front[0]);
   assert(e[1].equal(expected2.front[1]));
   expected2.popFront();
  }
}

struct SkipOverRet(alias pred, R) {
 R r;
 auto empty() { return r.empty; }
 auto front() { return r.front; }
 auto popFront() {
   auto x = r.front;
   do { r.popFront; } while(!r.empty && pred(x,r.front));
 }
 auto save() { return typeof(this)(r.save); }
}

auto skipOver(alias pred, R) (R r, bool autoSave=true) { return SkipOverRet!(pred,R)(autoSave?r.save:r); }

unittest{
  import std.algorithm.comparison : equal;
  import std.range : iota;
  assert(iota(10).skipOver!((x,y)=>y-x<2).equal([0,2,4,6,8]));
}

unittest
{
 import std.algorithm.comparison : equal;

 size_t popCount = 0;
 class RefFwdRange
 {
  int[]  impl;

  @safe:

   this(int[] data) { impl = data; }
  @property bool empty() { return impl.empty; }
  @property auto ref front() { return impl.front; }
  void popFront()
  {
   impl.popFront();
   popCount++;
  }
  @property auto save() { return new RefFwdRange(impl); }
 }
 static assert(isForwardRange!RefFwdRange);

 auto testdata = new RefFwdRange([1, 3, 5, 2, 4, 7, 6, 8, 9]);
 auto groups = testdata.myChunkBy!((a,b) => (a % 2) == (b % 2));
 auto outerSave1 = groups.save;

 // Sanity test
 assert(groups.equal!equal([[1, 3, 5], [2, 4], [7], [6, 8], [9]]));
 //assert(groups.empty);

 // Performance test for single-traversal use case: popFront should not have
 // been called more times than there are elements if we traversed the
 // segmented range exactly once.
 assert(popCount == 9);

 // Outer range .save test
 groups = outerSave1.save;
 assert(!groups.empty);

 // Inner range .save test
 auto grp1 = groups.front.save;
 auto grp1b = grp1.save;
 assert(grp1b.equal([1, 3, 5]));
 assert(grp1.save.equal([1, 3, 5]));

 // Inner range should remain consistent after outer range has moved on.
 groups.popFront();
 assert(grp1.save.equal([1, 3, 5]));

 // Inner range should not be affected by subsequent inner ranges.
 assert(groups.front.equal([2, 4]));
 assert(grp1.save.equal([1, 3, 5]));
}

