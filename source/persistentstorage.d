module persistentstorage;
import std.conv : to;
import std.algorithm : map, min, max;
import std.range : array, iota, drop, join, chunks;
import std.string : indexOf;
import std.stdio;
import std.file;
import std.path : baseName;
import std.format;
import tinyredis;
import sumtype;
import std.typecons : tuple;
import directexceptions;
import graylib.logger;
import std.exception;

mixin template PersistentStoreNT() {
 ExOk initializeNT() {
  return lift!(()=>initialize());
 }
 ExOk storeBinaryNT(K k, ubyte[] dat) {
  return lift!(()=>storeBinary(k,dat));
 }
 ExceptionOr!(ubyte[]) retrieveBinaryNT(K k) {
  return lift!(()=>retrieveBinary(k));
 }
 ExOk storeHashBinaryNT(K k, K hk, ubyte[] dat) {
  return lift!(()=>storeHashBinary(k,hk,dat));
 }
 ExceptionOr!(ubyte[]) retrieveHashBinaryNT(K k, K hk) {
  return lift!(()=>retrieveHashBinary(k,hk));
 }
 ExOk addToStreamBinaryNT(K k, ubyte[] dat) {
  return lift!(()=>addToStreamBinary(k,dat));
 }
 ExceptionOr!(ubyte[][]) getStreamBinaryNT(K k,ulong minIndex=0, ulong maxIndex=ulong.max, uint count=uint.max) {
  return lift!(()=>getStreamBinary(k,minIndex,maxIndex,count));
 }
 ExOk clearNT() {
  return lift!(()=>clear());
 }
}

mixin template FileBasedPersistentStoreParameters() {
 auto storeDir = "store";
}

mixin template PersistentStoreFunctionality() {
 mixin FileBasedPersistentStoreParameters;
 auto addPersistentStoreOptions(Args...)(Args args) {
  return tuple(args,"storeDir", &storeDir);
 }
}

auto makePersistentStore() {
 return makeFileBasedPersistentStore();
}

auto makeFileBasedPersistentStore(string storeDir="store") {
 return FileBasedPersistentStore(storeDir);
}

struct FileBasedPersistentStore {
 alias K = string; //Keys are strings

 mixin FileBasedPersistentStoreParameters;
 bool ready() {
  return true;
 }

 Ok initialize() {
  mkdirRecurse(storeDir);
  return Ok.init;
 } 
 private Ok storeBinaryPriv(K k, ubyte[] dat) {
   mkdirRecurse(storeDir);
   auto f = File(format!"%s/%s"(storeDir,k), "w");
   f.rawWrite(dat);
   f.close();
   return Ok.init;
 }
 Ok storeBinary(K k, ubyte[] dat) {
  return storeBinaryPriv(k, dat);
 }
 ubyte[] retrieveBinary(K k) {
  auto f  = File(format!"%s/%s"(storeDir,k), "r");
  ubyte[] ret;
  ret.length = f.size;
  f.rawRead(ret);
  f.close;
  return ret;
 }
 Ok storeHashBinary(K k, K hk, ubyte[] dat) {
  auto dir = format!"%s/%s"(storeDir,k);
  if (!dir.exists) { dir.mkdirRecurse; }
  auto hashKey = format!"%s/%s"(k,hk);
  storeBinary(hashKey,dat);
  return Ok.init;
 }
 ubyte[] retrieveHashBinary(K k, K hk) {
  auto hashKey = format!"%s/%s"(k,hk);
  return retrieveBinary(hashKey);
 }
 auto retrieveAllHashBinary(K k) {
  return format("%s/%s/",storeDir,k).dirEntries(SpanMode.shallow).array.map!baseName.map!(x=>tuple(x,retrieveHashBinary(k,x)));
 }
 Ok addToStreamBinary(K k, ubyte[] dat) {
  auto dir = format!"%s/%s"(storeDir,k);
  auto header = format!"%s/%s"(dir,"header");
  auto headerKey = format!"%s/header"(k);
  ubyte[] nextIndex= [0];
  if (!dir.exists) { dir.mkdirRecurse; }
  if (header.exists) {  
   nextIndex = retrieveBinary(headerKey);
  }
  auto streamKey = format!"%s/%s"(k,nextIndex[0]);
  storeBinary(streamKey,dat);
  nextIndex[0]++;
  storeBinary(headerKey,nextIndex);
  return Ok.init;
 }

 ubyte[][] getStreamBinary(K k,ulong minIndex=0, ulong maxIndex=ulong.max, uint count=uint.max) {
  auto dir = format!"%s/%s"(storeDir,k);
  auto header = format!"%s/%s"(dir,"header");
  auto headerKey = format!"%s/header"(k);
  auto nextIndex = retrieveBinary(headerKey);
  return iota(max(0,minIndex),min(nextIndex[0],maxIndex,minIndex+count)).map!(i=>retrieveBinary(format!"%s/%s"(k,i))).array;
 }
 Ok expire(K k, ulong secs) {
  //To be implemented
  return Ok.init;
 }
 Ok clear() {
  if(storeDir.exists){ rmdirRecurse(storeDir); }
  return Ok.init;
 }
 mixin PersistentStoreNT;
}

auto makeRedisPersistentStore(string host="127.0.0.1",ushort port=6379) {
 return RedisPersistentStore(host,port);
}
 struct Record {
   string recordId;
   string name;
   ubyte[] data;
 }

struct RedisPersistentStore {
 alias K = string; //Keys are strings
 string host="127.0.0.1";
 ushort port = 6379;
 Redis redisDb;
 bool ready() {
  try {
  string response = (redisDb.send("LOLWUT")).toString();
  } catch (Exception e) {
   return false;
  }
  return true;
 }
 Ok clear() {
  string response = (redisDb.send("FLUSHALL")).toString();
  return Ok.init; 
 }
 Ok initialize() {
  redisDb = new Redis(host,port);
  return Ok.init;
 }
 bool existsKey(K k) {
  return redisDb.send(format!"EXISTS %s"(k)).to!int==1;
 }
 Ok deleteKey(string redisKey) {
	redisDb.send(format("DEL %s", redisKey));
	return Ok.init;
 }
 int get_XLEN(K k) {
	return to!int(redisDb.send(format("XLEN %s", k))); 
 }
 Ok storeBinary(K k, ubyte[] dat) {
  string response = (redisDb.send("SET", k, cast(string) dat)).toString();
  return Ok.init; 
 }
 ubyte[] retrieveBinary(K k) {
  auto v = (redisDb.send("GET",k)).value;
  if(v==null) { throw new Exception("Value doesn't exist"); }
  return cast(ubyte[]) v.to!string;
 }
 Ok storeHashBinary(K k, K hk, ubyte[] dat) {
  string response = (redisDb.send("HSET", k, hk, cast(string) dat)).toString();
  return Ok.init; 
 } 
 ubyte[] retrieveHashBinary(K k, K hk) {
  auto v = (redisDb.send("HGET",k,hk)).value;
  if(v==null) { throw new Exception("Value doesn't exist"); }
  return cast(ubyte[]) (v.to!string);
 }
 auto retrieveAllHashBinary(K k) {
  auto v = (redisDb.send("HGETALL",k));
  return v.map!(x=>x.value).chunks(2).map!array.map!(x=>tuple(x[0],cast(ubyte[]) (x[1].to!string)));
 } 
 Ok addToStreamBinary(K k, ubyte[] dat) {
  string response = (redisDb.send("XADD", k,"*", "data", cast(string) dat)).toString();
  return Ok.init; 
 }
 ubyte[][] getStreamBinary(K k,ulong minIndex=0, ulong maxIndex=ulong.max, uint count=uint.max) {
  return redisDb.send("XRANGE",k, format!"%s-%s"(minIndex,0),format!"%s-%s"(maxIndex,ulong.max),"COUNT",count)
         .map!(x=>cast(ubyte[])(x.drop(1).front.drop(1).front.to!string)).array;
 }
 Record getLatestRecordDb(K k) {
  Response response = (redisDb.send("XREVRANGE", k, "+", "-", "COUNT", 1));
  grayLogger.tracef("GG response: %s\n", response);
  enforce(!response.empty, new RedisResponseException("No records"));
  Record record;
  auto payload = response.front;
  record.recordId = payload.front.to!string;
  grayLogger.tracef("GG record.recordId: %s\n", record.recordId);
  payload.popFront;
  grayLogger.tracef("GG response.front: %s\n", payload.front);
  record.name = payload.front.front.to!string;
  record.data = cast(ubyte[])(payload.front.drop(1).front.to!string);
  return record;
 }

 Ok expire(K k, uint seconds) {
  string response = redisDb.send("EXPIRE", k, seconds).toString();
  return Ok.init; 
 }
 Ok persist(K k) {
  string response = redisDb.send("PERSIST", k).toString();
  return Ok.init; 
 }
 auto getAllKeys(string pattern="*") {
  return redisDb.send("KEYS",pattern).map!(k=>k.toString());
 }

 string save() {
   return redisDb.send("SAVE").toString();
 }

  void shutdown() {
    redisDb.send("SHUTDOWN");
  }

 mixin PersistentStoreNT; 
}


unittest{
 import std.getopt;
 void test(string[] args) {
  int option1;
  mixin PersistentStoreFunctionality;
  try {
   auto helpInformation = getopt(addPersistentStoreOptions(args, "option1", &option1).expand);
   if(helpInformation.helpWanted) {
    defaultGetoptPrinter("Usage information.", helpInformation.options);
    return;
   }
  } catch  (Exception e) {
   writeln(e,"\nFor more information use --help");
  }
  auto ps = makePersistentStore();
  ps.initializeNT().okay;
 }
 test(["progname", "--storeDir", "boggy"]);
}

unittest{
 import std.algorithm: equal, sort;
 auto ps = makePersistentStore();
 try {
  ps.clear();
  ps.initialize;
  ps.storeBinary("hello",[1,2,3,4]);
  assert(ps.retrieveBinary("hello").equal([1,2,3,4]));
  ps.storeHashBinary("cow","brown",[1]);
  assert(ps.retrieveHashBinary("cow","brown")==[1]);
  ps.storeHashBinary("cow","black",[1,2]);
  assert(ps.retrieveAllHashBinary("cow").array.sort.equal([tuple("brown",[1]),tuple("black",[1,2])].sort));
  ps.addToStreamBinary("dog",[1]);
  ps.addToStreamBinary("dog",[1,2]);
  assert(ps.getStreamBinary("dog").equal([[1],[1,2]]));
  ps.clear();
 } catch(Exception e) {
  writeln("The following unexpected exception occurred:", e);
 }
}
unittest {
 import std.algorithm: equal;
 auto ps = makePersistentStore();
 ps.initializeNT.okay;
 ps.clear();
 ps.storeBinaryNT("hello",[1,2,3,4]).okay;
 assert(ps.retrieveBinaryNT("hello").match!(
  (Exception e) { writeln(e); return false;},
  (ubyte[] d) => d.equal([1,2,3,4])));
 assert(ps.retrieveBinaryNT("dog").match!(
  (Exception e) => true,
  (ubyte[] d) => false));
 ps.clear();
}
version(redisTest) {
  unittest {
    import std.algorithm : equal,sort;
    auto ps = makeRedisPersistentStore("localhost",6390);
    try {
      ps.initialize;
      ps.clear;
      ubyte[] dat = [1,2,3];
      ps.storeBinary("hello",dat);
      auto d = ps.retrieveBinary("hello"); 
      assert(d.equal(dat));
      ps.storeHashBinary("cow","brown",[1]);
      assert(ps.retrieveHashBinary("cow","brown")==[1]);
      ps.storeHashBinary("cow","black",[1,2]);
      assert(ps.retrieveHashBinary("cow","black")==[1,2]);
      assert(ps.retrieveAllHashBinary("cow").array.sort.equal([tuple("brown",[1]),tuple("black",[1,2])].sort));
      ps.addToStreamBinary("dog",[1]);
      ps.addToStreamBinary("dog",[1,2]);
      auto ds = ps.getStreamBinary("dog");
      assert(ds==[[1],[1,2]]);
      ps.clear();
    } catch(Exception e) {
      writeln("The following unexpected exception occurred:", e);
    }
  }

  unittest {
    import std.algorithm: equal;
    auto ps = makeRedisPersistentStore("localhost",6390);
    ps.initializeNT.okay;
    ps.clear();
    ps.storeBinaryNT("hello",[1,2,3,4]).okay;
    assert(ps.retrieveBinaryNT("hello").match!(
          (Exception e) { writeln(e); return false;},
          (ubyte[] d) => d.equal([1,2,3,4])));
    assert(ps.retrieveBinaryNT("dog").match!(
          (Exception e) => true,
          (ubyte[] d) => false));
    ps.clear();
  }
}
