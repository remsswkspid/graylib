module graylib.svg;
import std.algorithm;
import std.range;
import std.format;
import std.sumtype;
import graylib.geometry;
auto svgTspan(double x, double dy, string text) {
 return format("<tspan x='%s' dy='%sem'> %s </tspan>\n", x, dy, text);
}
auto svgTextStart(double x, double y, string attributes="") {
 return format("<text x='%s' y='%s' class='normal' %s>\n", x, y, attributes);
}
auto svgTextEnd() { return "</text>"; }

auto svgText(R)(double x, double y, R lines,string attributes="") {
 auto svg = svgTextStart(x,y,attributes);
 zip(lines,only(0.6).chain(1.2.repeat())).each!(a=>svg~=svgTspan(x,a[1],a[0]));
 svg ~= svgTextEnd();
 return svg;
}

auto svgLine(double x1, double y1, double x2, double y2) {
 return format("<line x1='%s' y1='%s' x2='%s' y2='%s' style='stroke:rgb(0,0,0);stroke-width:0.1'/>\n", x1, y1, x2, y2);
}
auto svgRect(double x, double y, double width, double height) {
 return format("<rect x='%s' y='%s' width='%s' height='%s' fill='blue' fill-opacity='0.1'/>\n",x, y, width, height);
}
auto svgPolygon(R)(R points,string color="black") {
 return format("<polygon points='%s' style='stroke:%s;stroke-opacity:1;stroke-width:0.5;fill-opacity:0' />\n",points.fold!((r,x)=>r~=format("%s,%s ", x[0],x[1]))(""),color);
}
auto svgHeader() { return "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>\n"; }
auto svgStart(double width, double height, Box viewBox,int fontSize=13 ) {
 return format("<svg width='%smm' height='%smm' viewBox='%s %s %s %s' xmlns='http://www.w3.org/2000/svg'>\n<style>\n.normal { font: italic %spx sans-serif; }\n</style>\n", width, height, viewBox.xMin, viewBox.yMin, viewBox.width, viewBox.height, fontSize);
}
auto svgEnd(){ return "</svg>"; }

alias Element = SumType!(Line,Rect,Polygon,Text);

auto labeledPoly(R)(R r) {
  return only(Element(Polygon(r))).chain(r.enumerate.map!(t=>Element(Text(t[1],[format!"%s"(t[0])],"text-anchor=\"middle\" dominant-baseline=\"middle\""))));
}

struct VectorImage {
 Element[] elements;
 auto shiftBelowAndAdd(R)(R newElements) {
   auto bb = this.boundingBox;
   auto bb2 = newElements.boundingBox;
   foreach(e; newElements.map!(e=>e.match!(x=>Element(shift(x,Point(0.0,-bb2.yMax+bb.yMin)))))) { elements ~= e; }
 }
}
struct Line { Point p; Point q; }
alias Rect = Box;
struct Polygon { Point[] points; }
struct Text { Point p; string[] lines; string attributes; }

auto boundingBox(Element[] elements) {
 Box eb = Box.init;
 return elements.fold!((r,e)=>r.coverBoxes(e.match!(
  (Line l)=>graylib.geometry.boundingBox([l.p,l.q]),
  (Rect r)=>r,
  (Polygon p)=>graylib.geometry.boundingBox(p.points),
  (Text t)=>graylib.geometry.boundingBox([t.p]))))(eb);
}
auto boundingBox(VectorImage vectorImage) {
  return vectorImage.elements.boundingBox;
}
auto shift(Line l, Point offset) { return Line(l.p.add(offset),l.q.add(offset)); }
auto shift(Rect r, Point offset) { return Box(r.xMin+offset[0],r.xMax+offset[0],r.yMin+offset[1],r.yMax+offset[1]); }
auto shift(Polygon poly, Point offset) { return Polygon(poly.points.map!(p=>p.add(offset)).array); }
auto shift(Text t, Point offset) { return Text(t.p.add(offset),t.lines); }

auto invert(Point p, double maxY) { return Point(p[0],maxY-p[1]); }
auto invert(Line l, double maxY) { return Line(l.p.invert(maxY),l.q.invert(maxY)); }
auto invert(Rect r, double maxY) { return Box(r.xMin,r.xMax,maxY-r.yMax,maxY-r.yMin); }
auto invert(Polygon poly, double maxY) { return Polygon(poly.points.map!(p=>p.invert(maxY)).array); }
auto invert(Text t, double maxY) { return Text(t.p.invert(maxY),t.lines,t.attributes); }
auto invert(VectorImage vectorImage, double maxY) {
  return VectorImage(vectorImage.elements.map!(e=>e.match!(x=>Element(invert(x,maxY)))).array);
}
auto toSvgString(Line l){ return svgLine(l.p[0],l.p[1],l.q[0],l.q[1]); }
auto toSvgString(Rect r){ return svgRect(r.xMin,r.yMin,r.width,r.height); }
auto toSvgString(Polygon p){ return svgPolygon(p.points); }
auto toSvgString(Text t){ return svgText(t.p[0],t.p[1],t.lines, t.attributes); }
auto toSvgString(VectorImage vectorImage, int fontSize=13, bool invert=false, double hScale=1.0, double vScale=1.0) {
 auto bb = vectorImage.boundingBox;
 auto newVectorImage = invert?vectorImage.invert(bb.yMax+bb.yMin):vectorImage; 
 return svgHeader() ~
        svgStart(bb.width*hScale,bb.height*vScale,bb,fontSize) ~
        format!"%-(%s\n%)"(newVectorImage.elements.map!(e=>e.match!toSvgString)) ~
        svgEnd();
}

