module graylib.interpolation;
import std.format;

struct InterpStruct {
  string pattern;
  string[] items;
}

auto interp(string pattern) {
  InterpStruct interp;

  long last = 0;
  foreach(ref i; 0..pattern.length) {
    auto c = pattern[i];
    switch(c) {
      case '%':
        i++;
        if(i>=pattern.length) { assert(0, "Pattern ended unexpectedly"); }
        if(pattern[i]=='{') { assert(0,"Missing specifier"); }
        if(pattern[i]=='%') { i++; continue; }
        while(true) {
          if(i>=pattern.length) { assert(0, "Pattern ended unexpectedly"); }
          if(pattern[i]=='{') {break;}
          i++;
        }

        interp.pattern ~= pattern[last..i];
        i+=1;
        long openBraceCount = 1;
        long start = i;
        bool inString = false;
        string end;
        while(true) {
          if(i>=pattern.length) { assert(0, "Pattern ended while looking for closing '}'"); }
          if(!inString) {
            switch(pattern[i]) {
              case '{':  openBraceCount++; break;
              case '}':  openBraceCount--; break;
              case '`':  end = "`"; inString = true; break;
              case '"':  end = `"`; inString = true; break;
              default: break;
            }
          } else {
            if(pattern[i] == end[0] && i + end.length < pattern.length && pattern[i+1..i+end.length] == end[1..$]) { inString = false; }
          }

          if(openBraceCount==0) { i++; break; }
          i++;
        }
        if(start+1>=i) { import std.conv; assert(0, "Empty interpolated item at position:  "~i.to!string~" in:\n"~pattern); }
        interp.items ~= pattern[start..i-1];
        last = i;
        continue;
      default: break;
    }

  }
  if(last< pattern.length) {  interp.pattern ~= pattern[last..$]; }
  string ret = format("format(%(%s%),%-(%s,%))",[interp.pattern],interp.items);
  return ret;
}

unittest {
  int a = 12;
  int b = 13;
  assert(mixin(interp("%s{a} * %s{b} = %s{a*b}")) == "12 * 13 = 156");
}


unittest {

  string sectionName = "RFID Tag Admin ";
  string sectionNameTooltip = "Used to store all the tags that are used. A tag can have a name (eg Card Tag 1, etc). The Tag Name and the Tag Number (only decimal digits) must be unique.";
  string addNew = "Add a New RFID Tag";
  string addNewTooltip = "The Tag Name and the Tag Number (only decimal digits) must be unique";
  string tableName = "tags";
  string formAction = "addit" ~ tableName;
  string tableStruct = "tagDataStruct";
  
  string output = mixin(interp(q"EOS
                    <!-- The div containing the add and edit of a table -->
                    <div class="separatorDiv"></div>
                    <div class="filter">
                        <div class="tableTitle">
                            <div class="tableAdminTitle">
                                <h2 class="tableH2 tooltip">%s{sectionName}<span class="tooltiptext">%s{sectionNameTooltip}</span> </h2>
                            </div>
                        </div>
                        <div class="tableH4Div">
                            <h4 class="tableH4 tooltip">%s{addNew}<span class="tooltiptext">%s{addNewTooltip}</span> </h4>
                        </div>
                        <div class="table-editable tableAddDiv"><span class="table-add glyphicon glyphicon-plus"></span>
                            <form action="%s{formAction}" method="post" onsubmit="recordVerticalOffset()">
                                <!--|||EMIT|||input(type="hidden", name="nextPage", value="#{url}") -->
                                <!--|||TABLE|||%s{tableName}, [wads.%s{tableStruct}], rowErrs, Vis.all, wads.selDatas[%s{tableName}] -->
EOS"
));
  import std.stdio;
  writeln(output);
}
