module graylib.commandlineoptions;
import std.stdio;
import std.traits;
import std.meta;
import std.format;
import std.getopt;
import std.conv;
import std.algorithm;
import std.range;
import graylib.grayappstart;
import graylib.logger;
import std.experimental.logger : GLogLevel = LogLevel;

struct OnlyCommandLine{
}

struct ShortName {
  this(string v) { assert(v.length == 1, "Short means one character stupid!"); value=v; }
  string value;
}

struct LongName {
  this(string v) { assert(v.length > 1, "Long means more than one character stupid!"); value=v; }
  string value;
}

struct Description {
  string value;
}
struct Append {
}

struct CommandLineOption {
  string shortName;
  string longName;
  string description;
  bool isActualOption = false;
  bool append = false;
}

struct App{
  string APP_VERSION = "0.0.0";
  string APP_NAME = "Name?";
  GitHash appGitHash;
  string[] origCmdArgs;
  string[] examples;
}

private
auto attributesToCommandLineOption(U,T...)(U u,T t) {
  CommandLineOption clo;
  static foreach(x; t) {
    static if(is(typeof(x) == ShortName)) {
      clo.shortName = x.value;
    } else static if(is(typeof(x) == LongName)) {
      clo.longName = x.value;
      clo.isActualOption = true;
    } else static if(is(typeof(x) == Description)) {
      clo.description = x.value;
    }
    else static if(is(typeof(x) == Append)) {
      clo.append = true;
    }
  }
  static if(is (U == enum)) {
    clo.description ~= format!" Must be one of %(%s, %)."([EnumMembers!U]);
  }
  clo.description ~= format!" Default is %s."(u);
  clo.description ~= format!"(%s)"(U.stringof);
  return clo;
}
private
bool isOption(string a) { return a.length>1 && a[0]=='-'; }

auto getCommandLineOptions(S)(ref string[] args, ref S  s, string name="", string[] examples = null) {
  enum getAttrs(alias x) = attributesToCommandLineOption(mixin("S.init.",__traits(identifier,x)),__traits(getAttributes,x));
  enum commandLineOptions = staticMap!(getAttrs,S.tupleof);
  static auto generateGetOptLine() {
    string ret = "getopt(args,\n";
    static foreach(i, clo; commandLineOptions) {
      static if(clo.isActualOption) {
        ret ~= format!"%(%s%),%(%s%),&ret.%s,\n"(
                [clo.shortName?clo.longName~"|"~clo.shortName:clo.longName],
                [clo.description],
                __traits(identifier,S.tupleof[i]));
      }
    }
    ret ~= ")";
    return ret;
  }
  if(name=="") { name =args[0]; }
  auto ret = s;
  foreach(i,ref x; ret.tupleof) {
    static if(is(typeof(x) == T[], T) && !commandLineOptions[i].append) {
      x = (T[]).init;
    }
  }
    try {
      arraySep=",";
      auto helpInformation = mixin(generateGetOptLine());      
      bool showHelp = helpInformation.helpWanted;
      if (showHelp) {
        writefln("=============== %s ==========================\n",name);
        defaultGetoptPrinter("Usage information.", helpInformation.options);
        if(examples.length>0) {
          write(format!"Examples:\n %-(%s\n%)\n"(examples));
        }
        return false;
      }
    }
    catch(Exception e) {
      import std.stdio : writeln;
      writeln(e.msg, "\nFor more information use --help");
      return false;
    }
  foreach(i,ref x; ret.tupleof) {
    static if(is(typeof(x) == T[], T) && !commandLineOptions[i].append) {
      if(x.length == 0) { x = s.tupleof[i]; }
    }
  }
  s = ret;
  return true;
}

auto cliAppStart(S)(ref string[] args, GitHash gitHash, ref S  commandLineOptions) {
  bool cliAppStartFlag = true;
  commandLineOptions.origCmdArgs = args;
  commandLineOptions.appGitHash = gitHash;
  graylib.grayappstart.appStart(commandLineOptions.appLogPrefix, args, commandLineOptions.APP_VERSION, gitHash);
  if(!getCommandLineOptions(args, commandLineOptions, commandLineOptions.APP_NAME, commandLineOptions.examples)) { return false; }
  grayLogger.logLevel = commandLineOptions.appLogLevel;
  grayLogger.logToCommandLine = commandLineOptions.logToCmdLine;
  grayLogger.tracef(cliAppStartFlag, "Show original args: [%(%s, %)]", commandLineOptions.origCmdArgs);
  grayLogger.tracef(cliAppStartFlag, "Show remaining args: [%(%s, %)]",args);
  grayLogger.infof("Opts app-log-level:%s, mode:%s", commandLineOptions.appLogLevel, commandLineOptions.mode);
  //setCommandLineArgs(args); //Use this if you want to run vibe with args
  return true;
}


