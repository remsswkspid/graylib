module graylib.taggedunion;
import std.stdio;
import std.conv : to;
import std.meta : staticIndexOf, AliasSeq;
import std.traits : TemplateArgsOf, isInstanceOf;

struct TaggedUnion(Types...) {
  union Storage {
    static foreach(i, T; Types) {
      mixin("T x" ~ i.to!string ~ ";");  
    }
  }
  Storage storage;
  size_t tag= size_t.max;
  alias getTypes = AliasSeq!Types;
  static foreach(i, T; Types) {
    this(T x) {
      storage.tupleof[i] = x;
      tag = i;
    }
  }
  static auto toTag(T)() { return staticIndexOf!(T,Types); }
  bool contains(T)() {
    const size_t i = toTag!T;
    return tag==i; 
  }
  ref auto get(T)(string msg="Attempted to access the wrong type in taggedUnion",string file=__FILE__, ulong line= __LINE__) {
    const size_t i = toTag!T;
    if (tag != i) {
     string errMsg = file~"("~line.to!string~"): "~ msg;   
     assert(0,errMsg);
    }
    return storage.tupleof[i];
  }
  void set(T)(T x) { 
    const size_t i = toTag!T;
    tag = i;
    storage.tupleof[i] = x;
  }
}
auto match(alias f, TU)(TU tu) if(is(TU == TaggedUnion!T,T)) {
  with(TU) {
    final switch(tu.tag) {
      static foreach(T; getTypes) {
        case toTag!T: return f(tu.get!T);
      }
    }
  }
}

template WrappedAliasSeq(Ts...) {alias unwrap=Ts; }

auto matchOr(alias f, alias g,alias Types, TU)(TU tu) {
  with(TU) {
    switch(tu.tag) {
      static foreach(T; Types.unwrap) {
        case toTag!T: return f(tu.get!T);
      }
      default: return g();
    }
  }
}


