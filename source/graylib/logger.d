module graylib.logger;
// Logs to redis

import tinyredis;
import std.string;
import std.experimental.logger;
import std.datetime;
import std.stdio;
import persistentstorage;

const string grayLoggerVersion = "grayLoggerVersion_V0.0.0";

GrayLogger grayLogger;

static this() {
  grayLogger = new GrayLogger(LogLevel.info);
}

class GrayLogger : Logger
{
    this(LogLevel lv) @safe
    {
        super(lv);
    }
    RedisPersistentStore ps;
    string keyPrefix;
    bool initialized = false;
    bool redisDead = false;
    bool logToCommandLine = false;
    void init(string prefix,
        LogLevel ll=LogLevel.info,
        bool logToCmdLine = false,
        int expireTime=3*24*60*60, //seconds
        string host="localhost",
        ushort port=6380) {
      initialized=true;
      logLevel = ll;
      try {
        ps = makeRedisPersistentStore(host,port);
        ps.initialize();
      } catch (Exception e) {
       redisDead = true;
      }
      this.keyPrefix = prefix;
      this.logToCommandLine = logToCmdLine;
      SysTime sysTime = Clock.currTime();
      auto key = streamKey(cast(DateTime)sysTime);
      writeMsgToRedis(__FILE__,__LINE__,"logger.logger.initLogging","logger.logger","trace","NA",sysTime,"sharedLog",format("creating logging stream %s",key));
    }

    auto streamKey(DateTime dateTime) {
        return format("%s-LOG-%04d-%02d-%02d", this.keyPrefix, dateTime.year, dateTime.month, dateTime.day);
    }
    void writeMsgToRedis(string file, int line, string funcName, string moduleName, string logLevel, string threadId,SysTime timestamp, string logger, string msg) {
      SysTime sysTime = Clock.currTime();
      DateTime nowDateTime = cast(DateTime)sysTime;
      auto key = this.streamKey(nowDateTime); 
      auto data = format("version '%s' time '%02d-%02d-%02d.%03d' file '%s' line %d funcName '%s' moduleName '%s' logLevel '%s' threadId '%s' timestamp '%s' logger '%s' msg '%s'",
          grayLoggerVersion,
          nowDateTime.hour, nowDateTime.minute, nowDateTime.second,sysTime.fracSecs.total!"msecs",
          file,
          line,
          funcName,
          moduleName,
          logLevel,
          threadId,
          timestamp.toISOExtString(),
          logger,
          msg
          );
      bool redisFailed = false;
      if(!redisDead) {
        try {
          import std.exception : enforce;
          enforce(initialized,"Logger not initialized");
          ps.addToStreamBinary(key, cast(ubyte[]) data);
        } catch (Exception ex) {
          redisFailed = true;
          writeln("Logging to redis failed. Logging via writeln");
          writeln(ex.msg);
        }
      }
      if(redisDead || redisFailed || logToCommandLine) {
        writeln(file,":",line, " ",nowDateTime.hour, "-", nowDateTime.minute, "-", nowDateTime.second, "-", sysTime.fracSecs.total!"msecs", " | ", msg);
      }
    } 

    @trusted override void writeLogMsg(ref LogEntry payload) 
    {
        if(super.logLevel > payload.logLevel) { return; }
        writeMsgToRedis(
            payload.file,
            payload.line,
            payload.funcName,
            payload.moduleName,
            format("%s",payload.logLevel),
            format("%s",payload.threadId),
            payload.timestamp,
            format("%s",payload.logger),
            payload.msg
            );
    }


    void initLogging(string keyPrefix, RedisPersistentStore ps, int expireSecs, bool logToCmdLine) {
    }
}

auto initGrayLogger(string prefix,
                    LogLevel ll=LogLevel.info,
                    bool logToCmdLine = false,
                    int expireTime=3*24*60*60, //seconds
                    string host="localhost",
                    ushort port=6380) {
  grayLogger.init(prefix,ll,logToCmdLine,expireTime,host,port);
}


unittest {
 import std;
 import std.algorithm : map;
 import std.conv : to;
 auto log = new GrayLogger(LogLevel.all);
 auto expireTime = 60; //in seconds
 log.init("Test", LogLevel.all, true, expireTime);
 auto key = log.streamKey(cast(DateTime) Clock.currTime());
 key.writeln;
 sharedLog = cast(shared)log;
 info("Logging_is_great");
 version(redisTest) {
   auto r = ps.getStreamBinary(key).map!(x=>(cast(string) x).split);
   auto x = r.front;
   auto y = r.drop(1).front;
   assert(x[0]=="time");
   assert(x[2]=="line");
   assert(x[4]=="funcName");
   assert(x[5]=="initLogging");
   assert(x[6]=="moduleName");
   assert(x[7]=="logger");
   assert(y[0]=="time");
   assert(y[2]=="line");
   assert(y[4]=="funcName");
   assert(y[6]=="moduleName");
   assert(y[7]=="'logger.logger'");
   assert(y[8]=="logLevel");
   assert(y[9]=="'info'");
   assert(y[14]=="msg");
   assert(y[15]=="'Logging_is_great'");
 }
}
