module graylib.deepcopy;
struct Ptr { void* val; }

T deepCopy(T)(T x) {
  Ptr[Ptr] ptrs;
  return deepCopy(x, ptrs);
}
T deepCopy(T)(T x, ref Ptr[Ptr] ptrs) {
  import std.meta : staticMap;
  import std.traits : Unqual;
  static if (is(T == S*, S)) {
    if(auto w = Ptr((cast(void*) x)) in ptrs) {
      return cast(T) w.val;
    }
    Unqual!(S)* ret;
    if(x!= null) {
      ret = new S;
      ptrs[Ptr(cast(void*) x)] = Ptr(cast(void*) ret);
      *ret = (*x).deepCopy(ptrs);
    }
    return cast(T) ret;
  } else static if(is (T == S[], S)) {
    Unqual!(S)[] ret;
    ret.reserve(x.length);
    foreach(w; x) {
      ret ~= w.deepCopy(ptrs);
    }
    return cast(T)ret;
  } else static if(is (T == S[V], S, V)) {
    Unqual!(S)[Unqual!(V)] ret;
    foreach(k, v; x) {
      ret[k.deepCopy(ptrs)] = v.deepCopy(ptrs);
    }
    return cast(T)ret;
  } else static if(__traits(compiles,T.tupleof)) {
    alias S = staticMap!(Unqual,typeof(x.tupleof)); 
    S tmp = void; 
    foreach(i, w; x.tupleof) {
      tmp[i] = (cast() w).deepCopy(ptrs);
    }
    return T(cast(typeof(x.tupleof)) tmp);
  } else static if(is( T == class)) {
    static assert(0,"Classes are not supported.");
  } else {
   return x;
  }
}
unittest {
  struct A {
    int[] as;
  }
  struct B {
   A* a;
   double b;
  }
  auto b1 =  B(null,123.4);
  auto c1 = b1.deepCopy;
  assert(c1.a == null);
  assert(c1.b == 123.4);
  c1.b = 0.0;
  assert(b1.b == 123.4);
  
  auto b2 = B(new A([1,2,3]),11);
  auto c2 = b2.deepCopy;
  assert(c2.a != null); 
  assert(c2.a.as == [1,2,3]);
  c2.a.as[0] = 123;
  assert(b2.a.as == [1,2,3]); 
}

unittest {
  struct Node {
    int val;
    Node* next;
  }
  auto list = new Node(1, new Node(2, new Node(3, null)));
  auto copy = list.deepCopy;
  assert(copy.next != null && copy.next.next != null && copy.next.next.next == null);
  assert(copy.val == 1 && copy.next.val == 2 && copy.next.next.val == 3);
  copy.val = 2;
  copy.next.val = 3;
  copy.next.next.val = 4;
  assert(copy.val == 2 && copy.next.val == 3 && copy.next.next.val == 4);
  assert(list.val == 1 && list.next.val == 2 && list.next.next.val == 3);
  auto infList = new Node(1, new Node(2, null));
  infList.next.next = infList;
  assert(infList.val == 1 && infList.next.val == 2 && infList.next.next == infList);
  auto infListCopy = infList.deepCopy;
  assert(infListCopy.val == 1 && infListCopy.next.val == 2 && infListCopy.next.next == infListCopy);
}

unittest {
 string test = "test";
 auto x = test.deepCopy;
 x ~= " 2";
 assert(x=="test 2");
 assert(test=="test");
}

unittest {
  struct A {
   immutable int x;
  }
  immutable auto a = A(1123);
  auto b = a.deepCopy;
  immutable(int)[] x = [1,2,3];
  x ~= 4;
  auto y = x.deepCopy;
  immutable(int[]) u = [1,2,4];
  immutable(int[]) v = u.deepCopy;
  immutable z = [A(123),A(456)];
  auto t = z.deepCopy;
}

unittest {
 int[immutable int] v;
 v[1] = 2;
 auto w = v.deepCopy;
 w[2] = 3;
 assert(v.length == 1);
 assert(w.length == 2);
 assert(w[1] == 2);
}

unittest {
 immutable(int[immutable int]) v = [1: 4, 2:3];
 auto w = v.deepCopy;
 assert(w[1] == 4);
}

