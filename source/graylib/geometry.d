module graylib.geometry;
import std.algorithm;
import std.range;
import std.typecons;
import std.math; 
import rangefunctions.rangefunctions : slideToEnd;

auto mightIntersect(double s1, double e1, double s2, double e2) {
  return e1 >= s2 && e2 >= s1; 
}
auto order(I)(I i, I j) { return (i[0][0] < j[0][0] || (!(i[0][0]>j[0][0]) && i[1][0] < j[1][0])); }
auto intersectingIntervals(R)(R r) {
  alias ord  = order!(ElementType!R);
  auto sorted = r.array.sort!ord; //Don't mess up origin
  return sorted.slideToEnd
               .filter!(v=>!v.empty)
               .map!(v=>v.save.until!(j=>v.front[1][0]<j[0][0])
               .filter!(j=>mightIntersect(v.front[0][1],v.front[1][1],j[0][1], j[1][1]))
               .map!(j=>tuple(intersection(v.front,j,0.5),v.front,j))
               .filter!(intRetvj=>intRetvj[0].doIntersect&&intRetvj[0].isStrictlyBetweenBothPairs))
               .filter!(v=>!v.empty)
               .join;
}
unittest {
  import std.stdio;
  double[2][2][] r = [[[0,-2],[0,2]],[[-2,0],[2,0]]];
  writeln("+=+=+=+=+=+=+=+");
  r.intersectingIntervals.writefln!"%(%s\n%)";
  writeln("+=+=+=+=+=+=+=+");
}

struct Point { 
  double x;
  double y;
  //allow indexing and expansion
  alias expand = typeof(this).tupleof;
  alias expand this; 
}

unittest{
  Point p = Point(1.0,1.5);
  static auto add(double a, double b) { return a + b; }
  assert(add(p.expand)==2.5);
  assert(p[1] == 1.5);
  p[1] = 2;
  assert(p[1] == 2.0);
  p.y -= 1.0;
  double err = 0.0000000001;
  assert(p[1] <= 1.0+err && p[1]>=1-err);
}


struct Box {
  double xMin=0.0;
  double xMax=0.0;
  double yMin=0.0;
  double yMax=0.0;
  auto empty() { return xMin == xMax && yMin == yMax; }
  auto width() { return xMax - xMin; }
  auto height() { return yMax - yMin; }
  auto center() { return Point((xMin+xMax)/2,(yMin+yMax)/2); }
  auto toPolygon() {
    return [Point(xMin,yMin), Point(xMax,yMin), Point(xMax,yMax), Point(xMin,yMax)];
  }
}

auto inside(Point p, Box b, double err=0.0) {
  return p.x+err >= b.xMin && p.x-err <= b.xMax && p.y+err >= b.yMin && p.y-err <= b.yMax;
}

unittest{
  auto p = Point(1.0,1.0);
  auto q = Point(1.0,-1.0);
  auto r = Point(0.5,0.5);
  auto box = Box(0.0, 1.0,0.0,1.0);
  assert(inside(p,box,0.000_001));
  assert(!inside(q,box,0.000_001));
  assert(inside(r,box));
}

auto coverBoxes(Box b1, Box b2) {
  if(b1.empty) { return b2; }
  if(b2.empty) { return b1; }
  return Box(min(b1.xMin,b2.xMin),max(b1.xMax,b2.xMax),min(b1.yMin,b2.yMin),max(b1.yMax,b2.yMax));
}
unittest{
  auto box1 = Box(0.0,1.0,0.0,1.0);
  auto box2 = Box(1.0,2.0,0.0,3.0);
  auto box3 = coverBoxes(box1,box2);
  import std.stdio;
  box3.writeln;
  assert(box3==Box(0.0,2.0,0.0,3.0));
}

auto boundingBox(R)(R points) {
  if(points.empty) { return Box.init; }
  auto p0 = points.front;
  auto ret = Box(p0[0],p0[0],p0[1],p0[1]);
  points.popFront;
  foreach(p ; points) {
    if(ret.xMin > p[0]) { ret.xMin = p[0]; }
    if(ret.xMax < p[0]) { ret.xMax = p[0]; }
    if(ret.yMin > p[1]) { ret.yMin = p[1]; }
    if(ret.yMax < p[1]) { ret.yMax = p[1]; }
  }
  return ret;
}

unittest{
assert(boundingBox([Point(1,0),Point(0,1)]) == Box(0.0,1.0,0.0,1.0));
}

pragma(inline)
auto compwise(alias op, P,Q)(P p, Q q) {
  P ret;
  assert(q.length == p.length);
  assert(ret.length == p.length);
  static foreach(i; 0..p.length) { ret[i] = op(p[i],q[i]); }
  return ret;
}
pragma(inline)
auto myFold(alias f, P, S)(P p, S seed) {
 auto ret = seed;
 static foreach(i ; 0..p.length) { ret = f(ret,p[i]); }
 return ret;
}
pragma(inline)
auto myFold(alias f, P)(P p) {
 auto ret = p[0];
 static foreach(i ; 1..p.length) { ret = f(ret,p[i]); }
 return ret;
}
pragma(inline)
auto myMap(alias f,Q,P)(P p) {
  Q ret;
  assert(ret.length == p.length);
  static foreach(i; 0..p.length) { ret[i] = f(p[i]); }
  return ret;
}
pragma(inline)
auto add(P,Q)(P p, Q q) { return compwise!((x,y)=>x+y)(p,q); }
pragma(inline)
auto sub(P,Q)(P p, Q q) { return compwise!((x,y)=>x-y)(p,q); }
pragma(inline)
auto dot(P,Q)(P p, Q q) { return compwise!((x,y)=>x*y)(p,q).myFold!((x,y)=>x+y); }
auto mag(P)(P p) {  return sqrt(p.dot(p)); }
auto scalMul(T,P)(T t, P p) { return myMap!(x=>t*x,P)(p); }
auto matVecMul(Q_=void,M,P)(M m, P p) {
  static if(is(Q_==void)) { alias Q=P; }
  else {alias Q = Q_; }
  return m.myMap!(q=>q.dot(p),Q);
}
auto trans(N,M)(M m) {
  N ret;
  foreach(i;0..m.length) {
    foreach(j; 0..m[i].length) {
      ret[j][i] = m[i][j];
    }
  }
  return ret;
}
auto matMatMul(C,A,B)(A a, B b) { 
  alias Row = typeof(C.init[0]);
  alias BT = typeof(b[0][0])[B.length][typeof(B.init[0]).length];
  auto bT = b.trans!BT;
  return a.myMap!(r=>bT.myMap!(c=>r.dot(c),Row),C);
}
auto distBetweenPointsSqrd(P)(P a, P b) if(is(typeof(P.init[0]) ==double)) { auto v=a.sub(b); return v.dot(v); }
struct NearestPoint(P) {
  P p;
  double err;
  P point;
  double isPerp;
  double distSqrd() { return distBetweenPointsSqrd(p,point); }
}
auto nearestPoint(P)(P p, P a, P b, double err) {
  assert(a!=b);
  auto ret = NearestPoint!P(p,err);
  auto v = b.sub(a);
  auto w = p.sub(a);
  auto s = w.dot(v)/v.dot(v);
  if(s<-err) { ret.point = a; ret.isPerp=false; }
  else if(s>1+err) { ret.point = b; ret.isPerp=false; }
  else { ret.point = a.add(s.scalMul(v)); ret.isPerp=true; }
  return ret; 
}
struct NearestPoints(P) {
  P ab;
  P cd;
  auto distSqrd() { return distBetweenPointsSqrd(ab,cd); }
  auto dist() { return sqrt(distSqrd); }
}
auto nearestPoints(P)(P a, P b, P c, P d, double err) {
  assert(a!=b);
  assert(c!=d);
  auto tmp = [ nearestPoint(a,c,d,err), nearestPoint(b,c,d,err), nearestPoint(c,a,b,err), nearestPoint(d,a,b,err)]
              .enumerate
              .map!(t=>tuple(t[1].distSqrd,t))
              .fold!((t1,t2)=>t1[0]<=t2[0]?t1:t2)[1]; 
  switch(tmp[0]) {
    case 0: return NearestPoints!P(a,tmp[1].point);
    case 1: return NearestPoints!P(b,tmp[1].point);
    case 2: return NearestPoints!P(tmp[1].point,c);
    case 3: return NearestPoints!P(tmp[1].point,d);
    default: assert(0);
  }
}
unittest {
  import std.stdio;
  nearestPoints(Point(0,0),Point(3,-3),Point(1,0),Point(5,0),0.0000001).writeln;
}
//distance squared between line segments a -- b , c -- d
auto distSqrd(P)(P a, P b, P c, P d, double err) {
  return nearestPoints(a,b,c,d,err).distSqrd;
}
struct NearestPointsPolyPoint(P) {
  P p;
  P onPoly;
  size_t index; //index such that onPoly is between poly[index] and poly[(index+1)%$]
  auto distSqrd() { return distBetweenPointsSqrd(p,onPoly); }
  auto dist() { return sqrt(distSqrd); }
}
auto nearestPointsPolyPoint(R,P)(R poly, P p, double err) {
  auto tmp = iota(poly.length).map!(i=>tuple(i,nearestPoint(p,poly[i],poly[(i+1)%$],err)))
                          .fold!((t1,t2)=>(t1[1].distSqrd <= t2[1].distSqrd?t1:t2));
  return NearestPointsPolyPoint!P(p,tmp[1].point,tmp[0]);
}
//distance squred between polygon poly and a point p
auto distSqrd(R,P)(R poly,P p, double err) if(is(typeof(R.init.front[0])==typeof(P.init[0]))) {
  return nearestPointsPolyPoint(poly,p,err).distSqrd;
}
unittest {
 import std.format;
auto p = Point(3237, 27030);
auto poly = [Point(3240.17, 27485), Point(3240.17, 27485), Point(3271.96, 28605.7), Point(3326.87, 30341.8), Point(3398.16, 32540.6), Point(3481.01, 35047.2), Point(3570.61, 37707.6), Point(3660.2, 40368.1), Point(3745.94, 42873.7), Point(3821.09, 45071.5), Point(3881.78, 46807.7), Point(3922.24, 47927.3), Point(5834.54, 48859.5), Point(6348.02, 48151.2), Point(5847.07, 48870.3), Point(6343.21, 49107.1), Point(6772.87, 48438.3), Point(6356.69, 49118.9), Point(6754.57, 49309.3), Point(7170.75, 48662.2), Point(6764.2, 49305.4), Point(28826.5, 60689), Point(28826.5, 60689), Point(29126.1, 58559.3), Point(29547.1, 56144.4), Point(30069.3, 53525.4), Point(30672.3, 50781.1), Point(31338, 47991.4), Point(32044.2, 45235.3), Point(32773.5, 42593.5), Point(33503.7, 40144.2), Point(34215.6, 37969.1), Point(34890, 36147.1), Point(34890, 36147.1), Point(33846.7, 35347.1), Point(33049.9, 33916.7), Point(32467.1, 31968.5), Point(32063.5, 29609.9), Point(31807.2, 26952.4), Point(31663.6, 24106.4), Point(31599.1, 21180.6), Point(31580.8, 18287.4), Point(31576, 15535.2), Point(31550.9, 13035.5), Point(26684.9, 13095.7), Point(26684.9, 13095.7), Point(24267.8, 13054.2), Point(21930.6, 12907.3), Point(19652.3, 12680.4), Point(17410.5, 12397.3), Point(15185.1, 12081.6), Point(12953.9, 11760), Point(10696.7, 11454.2), Point(8390.38, 11190.8), Point(6014.7, 10993.6), Point(3548.45, 10888), Point(3548.45, 10888), Point(3410.69, 12945.7), Point(3312.42, 14901.9), Point(3246.91, 16762.3), Point(3208.38, 18530), Point(3192, 20210), Point(3192, 21809), Point(3201.63, 23331.1), Point(3217.05, 24781.2), Point(3231.5, 26164.2), Point(3240.17, 27485)].removeRepeats;
 foreach(e; poly.pairs) {
   assert(!nearestPoint(p,e[0],e[1],0.00001).distSqrd.isNaN, format!"%s %s"(e,p));
 }
 auto n = nearestPointsPolyPoint(poly, p, 0.00001);
 assert(!n.distSqrd.isNaN);
}
struct NearestPointsPolyLine(P) {
  P ab;
  P onPoly;
  size_t index; //index such that onPoly is between poly[index] and poly[(index+1)%$]
  auto distSqrd() { return distBetweenPointsSqrd(ab,onPoly); }
  auto dist() { return sqrt(distSqrd); }
}
auto nearestPointsPolyLine(R,P)(R poly, P a, P b, double err) {
  auto tmp = iota(poly.length).map!(i=>tuple(i,nearestPoints(a,b,poly[i],poly[(i+1)%$],err)))
                          .fold!((t1,t2)=>(t1[1].distSqrd <= t2[1].distSqrd?t1:t2));
  return NearestPointsPolyLine!P(tmp[1].ab,tmp[1].cd,tmp[0]);
}
//distance squred between polygon poly and line segment a-- b
auto distSqrd(R,P)(R poly,P a, P b, double err) if(is(typeof(R.init.front[0])==typeof(P.init[0]))) {
  return nearestPointsPolyLine(poly,a,b,err).distSqrd;
}
unittest {
  auto err = 0.00000001;
  auto p = Point(0,0);
  auto a = Point(1,0);
  auto b = Point(0,1);
  auto d = nearestPoint(p,a,b,err).distSqrd();
  assert(d>=0.5-err && d<=0.5+err);
  p = Point(1,0);
  d = nearestPoint(p,a,b,err).distSqrd;
  assert(d>=0.0-err && d<=0.0+err);
  p = Point(0.5,0.5);
  d = nearestPoint(p,a,b,err).distSqrd;
  assert(d>=0.0-err && d<=0.0+err);
  p = Point(2,0);
  d = nearestPoint(p,a,b,err).distSqrd;
  assert(d>=1-err && d<=1+err);
}
unittest {
  auto err = 0.00000001;
  auto a = Point(1,0);
  auto b = Point(0,1);
  auto c = Point(2,0);
  auto d = Point(1,1);
  auto r = distSqrd(a,b,c,d,err);
  assert(r>=0.5-err && r<=0.5+err);
}
unittest {
  auto err = 0.00000001;
  auto r = distSqrd([Point(0,1),Point(2,1),Point(0,3)],Point(1,0),Point(1,-1),err);
  assert(r>=1-err && r<= 1+err);
}
//check distance is within
auto within(P)(P p, P q,double r) {
  return (x=>x.dot(x))(p.sub(q)) < r*r;
}
struct IntersectionRet(P) {
  double s;
  double t;
  P v;
  P w;
  P p1;
  P q1;
  double err;
  bool doIntersect() { return !s.isNaN && !t.isNaN; }
  P intersectionPoint() { return p1.add(t.scalMul(v)); }
  bool isAtFirst() { return -err < t && t < err; }
  bool isAtSecond() { return 1-err < t && t < 1+err; }
  bool isAtThird() { return -err < s && s < err; }
  bool isAtForth() { return 1-err < s && s < 1+err; }
  bool isBetweenFirstPair() { return t>-err && t<1+err; }
  bool isBetweenSecondPair() { return s>-err && s<1+err; }
  bool isBetweenBothPairs() { return isBetweenFirstPair && isBetweenSecondPair; }
  bool isStrictlyBetweenFirstPair() { return t>err && t<1-err; }
  bool isStrictlyBetweenSecondPair() { return s>err && s<1-err; }
  bool isStrictlyBetweenBothPairs() { return isStrictlyBetweenFirstPair && isStrictlyBetweenSecondPair; }
  alias expand = typeof(this).tupleof;
}
auto intersection(P)(P p1,P p2, P q1, P q2,double err) {
  auto v = p2.sub(p1);
  auto w = q2.sub(q1);
  P vPerp, wPerp;
  vPerp[0] =-v[1]; vPerp[1] = v[0];
  wPerp[0] =-w[1]; wPerp[1] = w[0];
  auto u = p1.sub(q1);
  auto v_dot_wPerp = v.dot(wPerp);
  auto u_dot_wPerp = u.dot(wPerp);
  double s,t;
  if(v_dot_wPerp >= -err && v_dot_wPerp <=err && u_dot_wPerp >= -err && u_dot_wPerp <=err) {
     (){
       s = w.dot(u);
       if(s>=-err && s<=1+err) {
         t=0;
         return;
       }
       s = w.dot(p2.sub(q1));
       if(s>=-err && s<=1+err) {
         t=1;
         return;
       }
       t = -v.dot(u);
       if(t>=-err && t<=1+err) {
         s=0;
         return;
       }
       t = v.dot(q2.sub(p1));
       s = 1;
   
     }();
  } else {
    //p1 + t*v = q1+s*w
    //p1*vPerp + t*v*vPerp = q1*vPerp + s*w*vPerp
    //s = u*vPerp/w*vPerp
    //t = -u*wPerp/v*wPerp
    s = u.dot(vPerp)/w.dot(vPerp);
    t = -u.dot(wPerp)/v.dot(wPerp);
  }
  return IntersectionRet!P(s,t,v,w,p1,p2,err);
}
auto intersection(I,J)(I i, J j, double err) { return intersection(i[0],i[1],j[0],j[1],err); }
auto pairs(R)(R r) { return r.chain(r[0..1]).slide(2); }
auto triples(R)(R r) {
  return r[$-1..$].chain(r).chain(r[0..1]).slide(3);
}
auto isCcw(P)(P points) {  return points.pairs.map!(t=>(t[1][0]-t[0][0])*(t[1][1]+t[0][1])).sum <0; }

auto notClose(Box a, Box b) {
  return a.xMax < b.xMin || b.xMax < a.xMin || a.yMax < b.yMin || b.yMax < a.yMin;
}

bool between(double a, double b, double c) { return (b <= a && a<= c) || (c<=a && a<=b); }

double xIntercept(double y, Point a, Point b){
 return (y-a[1])*(b[0]-a[0])/(b[1]-a[1]) + a[0];
}

auto scanLine(RP)(double y, RP points) {
 struct xEdge {double x; Point s; Point e; }
 auto xEdges = pairs(points)
  .filter!(a=>a[0][1]!=a[1][1] && between(y,a[0][1],a[1][1]))
  .map!(a=>xEdge(xIntercept(y,a[0],a[1]),a[0],a[1]))
  .filter!(a=>between(a.x,a.s[0],a.e[0])).array;
 bool[] keep;
 keep.length = xEdges.length;
 for(auto i=0; i < xEdges.length; i++) { keep[i] = true; }
 for(auto i=0; i < xEdges.length; i++) {
  auto pi = (i==0?xEdges.length-1:i-1);
  if (y != xEdges[i].s[1]) {
  } else if ((xEdges[i].e[1] <= y && y <= xEdges[pi].s[1]) || (xEdges[i].e[1] >= y && y >= xEdges[pi].s[1])) {
   keep[i] = false;
  } else {
   keep[i]=false;
   keep[pi]=false;
  }}
 auto ret = xEdges.map!(a=>a.x).enumerate.filter!(a=>keep[a[0]]).map!(a=>a[1]).array.sort;
// writeln(y);
// writeln(xEdges);
// writeln(ret);
 return ret;
}
auto inside(RP)(Point p, RP poly) {
  bool inside = false;
  foreach(x; scanLine(p[1],poly)) {
   if(x <= p[0]) { inside = !inside; }
   else { break; }
  }
  return inside;
}
unittest {
  Point[] poly = [ Point(5,0), Point(15,5), Point(25,0), Point(30,10), Point(0,10)];
  assert(Point(5,2.5).inside(poly));
  assert(!Point(5.1,0.0).inside(poly));
  assert(!Point(15,4.9).inside(poly));
}

auto polygonsOverlap(R1, R2)(R1 r1, R2 r2, double err, ref size_t rei, ref size_t rej) {
  if(notClose(r1[].boundingBox,r2[].boundingBox)) { return false; }
  if(polygonsIntersect!true(r1,r2,err,rei,rej)) { return true; }
  return r1.front.inside(r2) || r2.front.inside(r1); 
}
auto intersects(R)(R e, Box box,double err) {
  if(e[0].inside(box) || e[1].inside(box)) { return true; }
  auto a = Point(box.xMin,box.yMin);
  auto b = Point(box.xMin,box.yMax);
  auto c = Point(box.xMax,box.yMax);
  auto d = Point(box.xMax,box.yMin);
  auto i = intersection(e,tuple(a,b),err);
  if(i.doIntersect && i.isBetweenBothPairs) { return true; }
  i = intersection(e,tuple(b,c),err);
  if(i.doIntersect && i.isBetweenBothPairs) { return true; }
  i = intersection(e,tuple(c,d),err);
  if(i.doIntersect && i.isBetweenBothPairs) { return true; }
  i = intersection(e,tuple(d,a),err);
  if(i.doIntersect && i.isBetweenBothPairs) { return true; }
  return false;
}
//Slow version
auto polygonsIntersect(bool skipCloseCheck=false, R1, R2)(R1 r1, R2 r2,double err, ref size_t rei, ref size_t rej) {
  static if(!skipCloseCheck) {
    if(notClose(r1[].boundingBox,r2[].boundingBox)) { return false; }
  }
  auto bb1 = r1[].boundingBox;
  auto bb2 = r2[].boundingBox;
  size_t nrei =0; 
  size_t nrej =0;
  foreach(e1; r1[].pairs) {
    nrei++;
    assert(e1[0]!=e1[1]);
    if(!e1.intersects(bb2,err)) { continue; }
    nrej =0;
    foreach(e2; r2[].pairs) {
      nrej++;
      assert(e2[0]!=e2[1]);
      auto i = intersection(e1,e2,err);
      if(i.doIntersect && i.isBetweenBothPairs) {
        rei = nrei;
        rej = nrej;
        return true;
      }
    }
  }
  return false;
}

auto polygonsTheSame(R1, R2)(R1 poly1, R2 poly2, double err) {
  foreach(p; poly1) {
     if(distSqrd(poly2,p,err)>err) { return false; }
  }
  foreach(p; poly2) {
     if(distSqrd(poly1,p,err)>err) { return false; }
  }
  return true;
}

//NOT GOOD AS STANDS
auto polygonsPseudoTheSame(R1, R2)(R1 poly1, R2 poly2, double err) {
  auto sumSqrs = 0.0;
  foreach(p; poly1) {
      sumSqrs+=distSqrd(poly2,p,err);
  }
  if(sumSqrs/poly1.length > 10_000_000) { return false; }
  sumSqrs = 0.0;
  foreach(p; poly2) {
      sumSqrs+=distSqrd(poly2,p,err);
  }
  return sumSqrs/poly1.length <= 10_000_000;
}

auto scaleAndShiftToMakeBoundingBox(R)(R poly, Box b) {
  alias P = ElementType!R;
  auto bb = poly.boundingBox;
  auto c = bb.center;
  auto nc = b.center;
  auto scal = Point(b.width/bb.width, b.height/bb.height);
  auto transform(P p) {
     return p.sub(c).compwise!((x,y)=>x*y)(scal).add(nc);
  }
  return poly.map!transform;
}

auto removeRepeats(R)(R poly) {
  auto ret = poly.uniq.array;
  if(ret.length > 1 && ret[0] == ret[$-1]) { return ret[0..$-1]; }
  return ret;
}

unittest {
  import graylib.svg;
  auto poly1 = [Point(10,0), Point(20,20), Point(30,10), Point(20,10)];
  auto poly2 = [Point(20,10), Point(10,0), Point(20,20), Point(30,10)];
  auto vectorImage = VectorImage([
    Element(Polygon(poly1)),
    Element(Polygon(poly2.scaleAndShiftToMakeBoundingBox(poly1.boundingBox).array)),
    Element(Polygon(poly1.boundingBox.toPolygon))]);
  import std.file;
  std.file.write("tmp.svg", vectorImage.toSvgString);
  assert(poly1.polygonsTheSame(poly2,0.00001));
}
unittest {
  import graylib.svg;
  auto poly1 = [Point(10,0), Point(20,20), Point(30,10), Point(20,10)];
  auto poly2 = [Point(2,6), Point(1,1), Point(2,11), Point(3,6)];
  auto vectorImage = VectorImage([
    Element(Polygon(poly1)),
    Element(Polygon(poly2.scaleAndShiftToMakeBoundingBox(poly1.boundingBox).array)),
    Element(Polygon(poly1.boundingBox.toPolygon))]);
  import std.file;
  std.file.write("tmp2.svg", vectorImage.toSvgString);
}
auto polygonsSimilar(R)(R poly1, R poly2, double err) {
  return poly1.polygonsTheSame(
           poly2.scaleAndShiftToMakeBoundingBox(poly1.boundingBox)
                .array,
           err);
}
auto polygonsPseudoSimilar(R)(R poly1, R poly2, double err) {
  return poly1.polygonsPseudoTheSame(
           poly2.scaleAndShiftToMakeBoundingBox(poly1.boundingBox)
                .array,
           err);
}

unittest {
  auto poly1 = [Point(0,0),Point(1,0),Point(0,2)];
  auto poly2 = [Point(1,1),Point(3,1),Point(1,7)];
  assert(polygonsSimilar(poly1,poly2,0.00001));
  poly2 = [Point(2,1),Point(3,1),Point(1,7)];
  assert(!polygonsSimilar(poly1,poly2,0.0001));
}
unittest {
import graylib.svg;
auto poly1 = [Point(104995, 137486), Point(95892, 137486), Point(95892, 137191), Point(96088, 137191), Point(95892, 137191), Point(95695, 137191), Point(95892, 137191), Point(95892, 137486), Point(95443, 137486), Point(95443, 137191), Point(95637, 137191), Point(95443, 137191), Point(95245, 137191), Point(95443, 137191), Point(95443, 137486), Point(94968, 137486), Point(94968, 137191), Point(95162, 137191), Point(94968, 137191), Point(94770, 137191), Point(94968, 137191), Point(94968, 137486), Point(83987, 137486), Point(83987, 137191), Point(84184, 137191), Point(83987, 137191), Point(83789, 137191), Point(83987, 137191), Point(83987, 137486), Point(69407, 137486), Point(69407, 137191), Point(69604, 137191), Point(69407, 137191), Point(69209, 137191), Point(69407, 137191), Point(69407, 137486), Point(54801, 137486), Point(54801, 137191), Point(54998, 137191), Point(54801, 137191), Point(54605, 137191), Point(54801, 137191), Point(54801, 137486), Point(51476, 137486), Point(51500, 132385), Point(54750, 132385), Point(54750, 132680), Point(54556, 132680), Point(54750, 132680), Point(54948, 132680), Point(54750, 132680), Point(54750, 132385), Point(69382, 132385), Point(69382, 132680), Point(69184, 132680), Point(69382, 132680), Point(69579, 132680), Point(69382, 132680), Point(69382, 132385), Point(83987, 132385), Point(83987, 132680), Point(83789, 132680), Point(83987, 132680), Point(84184, 132680), Point(83987, 132680), Point(83987, 132385), Point(94968, 132385), Point(94968, 132680), Point(94770, 132680), Point(94968, 132680), Point(95162, 132680), Point(94968, 132680), Point(94968, 132385), Point(95417, 132385), Point(95417, 132680), Point(95220, 132680), Point(95417, 132680), Point(95613, 132680), Point(95417, 132680), Point(95417, 132385), Point(95843, 132385), Point(95843, 132680), Point(95645, 132680), Point(95843, 132680), Point(96037, 132680), Point(95843, 132680), Point(95843, 132385), Point(104995, 132385)];

auto poly2 =[Point(97566, 72120), Point(87971, 72120), Point(87971, 71825), Point(88165, 71825), Point(87971, 71825), Point(87773, 71825), Point(87971, 71825), Point(87971, 72120), Point(87520, 72120), Point(87520, 71825),Point(87715, 71825), Point(87520, 71825), Point(87323, 71825), Point(87520, 71825), Point(87520, 72120), Point(87045, 72120), Point(87045, 71825), Point(87240, 71825), Point(87045, 71825), Point(86848, 71825), Point(87045, 71825), Point(87045, 72120), Point(75573, 72120), Point(75573, 71825), Point(75770, 71825), Point(75573, 71825), Point(75375, 71825), Point(75573, 71825), Point(75573, 72120), Point(60499, 72120), Point(60499, 71825), Point(60697, 71825), Point(60499, 71825), Point(60302, 71825), Point(60499, 71825), Point(60499, 72120), Point(45404, 72120), Point(45404, 71825), Point(45599, 71825), Point(45404, 71825), Point(45207, 71825), Point(45404, 71825), Point(45404, 72120), Point(42080, 72120), Point(42104, 67019), Point(45354, 67019), Point(45354, 67314), Point(45157, 67314), Point(45354, 67314), Point(45549, 67314), Point(45354, 67314), Point(45354, 67019), Point(60474, 67019), Point(60474, 67314), Point(60277, 67314), Point(60474, 67314), Point(60672, 67314), Point(60474, 67314), Point(60474, 67019), Point(75573, 67019), Point(75573, 67314), Point(75375, 67314), Point(75573, 67314), Point(75770, 67314), Point(75573, 67314), Point(75573, 67019), Point(87045, 67019), Point(87045, 67314), Point(86848, 67314), Point(87045, 67314), Point(87240, 67314), Point(87045, 67314), Point(87045, 67019), Point(87496, 67019), Point(87496, 67314), Point(87298, 67314), Point(87496, 67314), Point(87690, 67314), Point(87496, 67314), Point(87496, 67019), Point(87920, 67019), Point(87920, 67314), Point(87723, 67314), Point(87920, 67314), Point(88116, 67314), Point(87920, 67314), Point(87920, 67019), Point(97566, 67019)];
  auto vectorImage = VectorImage([
    Element(Polygon(poly1)),
    Element(Polygon(poly2.scaleAndShiftToMakeBoundingBox(poly1.boundingBox).array)),
    Element(Polygon(poly1.boundingBox.toPolygon))]);
  import std.file;
  std.file.write("tmp3.svg", vectorImage.toSvgString(13,false,0.1,0.1));

}
