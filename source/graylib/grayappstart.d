module graylib.grayappstart;
import graylib.logger;
import std.experimental.logger : GLogLevel = LogLevel;

struct GitHash {
  string currentGitHash;
  string uncommittedChanges;
}

void appStart(string appLogPrefix, string[] args, string versionString, GitHash gitHash) {
  initGrayLogger(appLogPrefix, GLogLevel.info);
  grayLogger.infof("Starting: %s, version:%s", appLogPrefix, versionString);
  grayLogger.infof("Show args: [%(%s, %)]",args);
  grayLogger.infof("GitHash %s", gitHash.currentGitHash);
  if(gitHash.uncommittedChanges != "") {
    grayLogger.infof("Uncommitted Changes %s", gitHash.uncommittedChanges);
  }

}
