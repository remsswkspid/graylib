module containers.circularbuffer;
import std.range : ElementType;
import std.typecons : Tuple, tuple;
//modular adders and subtractors for sircular buffer
struct AddMod {
 ulong base;
 ulong opCall(ulong a, ulong b) {
  return (a +b)%base;
 }
}

auto makeAdder(ulong base) {
 AddMod ret;
 ret.base = base;
 return ret;
}

struct SubMod {
 ulong base;
 ulong opCall(ulong a, ulong b) {
  return (a + (base - (b%base)))%base;
 }
}
auto makeSubber(ulong base) {
 SubMod ret;
 ret.base = base;
 return ret;
}
unittest {
 auto add17 = makeAdder(17);
 auto sub17 = makeSubber(17);
 static foreach(i; 0..17) {
  static foreach(j ; 0..17) {
   assert(add17(sub17(i,j),j)==i);
   assert(sub17(add17(i,j),j)==i);
  }
 }
 assert(add17(15,2) == 0);
 assert(add17(15,12) == 10);
 assert(add17(15,34) == 15);
 assert(sub17(15,34) == 15);
 assert(sub17(12,15) == 14);
}
struct CircularBufferSlice(R) {
 CircularBuffer!R cb;
 ulong ci;
 ulong ei;
 auto empty() { return ci>=ei; }
 auto front() { return cb[ci]; }
 auto popFront() { ci++; }
 auto save() { return typeof(this)(cb,ci,ei); }
}

struct CircularBuffer(R) {
 alias T = ElementType!R;
 ulong capacity;
 R buff;
 ulong f;
 ulong b;
 AddMod add;
 SubMod sub;
 ulong length=0;
 void incFrontBy(ulong n) { f = add(f,n); }
 void decFrontBy(ulong n) { f = sub(f,n); }
 void incBackBy(ulong n) { b = add(b,n); }
 void decBackBy(ulong n) { b = sub(b,n); }
 auto full() { return length==capacity; }
 auto empty() { return f==b; }
 auto front() { return buff[f]; }
 auto back() { return buff[sub(b,1)]; }
 void popFront() { assert(!empty()); incFrontBy(1); length--; }
 void popBack() { assert(!empty()); decBackBy(1); length--; }
 void popNFront(ulong n) { assert(length>=n); incFrontBy(n); length-=n; }
 void popNBack(ulong n) { assert(length>=n); decBackBy(n); length-=n; }
 auto pushBack(T x) {
  buff[b] = x;
  incBackBy(1);
  if (f==b) { incFrontBy(1); }
  else { length++; }
 }
 auto pushFront(T x) {
  decFrontBy(1);
  if (f==b) { decBackBy(1); }
  else { length++; }
  buff[f] = x;
 }
 auto append(T x) { return pushBack(x); }
 auto prepend(T x) { return pushFront(x); }
 auto save() {
  return CircularBuffer!R(capacity,buff.dup,f,b,add,sub,length);
 }
 void reset(ulong c) {
  capacity = c;
  length = 0;
  if (buff.length < c) {
   buff.length = c +1;
  }
  add = makeAdder(c+1);
  sub = makeSubber(c+1);
  clear();
 }
 void clear() {
  f=b=0;  
 }
 T opIndex(ulong index) { return buff[add(f,index)]; }
 T opIndexAssign(T x, ulong index) {
  buff[add(f,index)] = x;
  return x;
 }
 auto opSlice(ulong s, ulong e) {
  return CircularBufferSlice!R(this,s,e); }
}
///Make a circular buffer with capacity c
auto makeCircularBuffer(R)(ulong c) {
 CircularBuffer!R ret;
 ret.reset(c);
 return ret;
}
//Make a circular buffer which uses range r as underlying buffer
//NB length of R must be one greater than the desired capacity
auto makeCircularBuffer(R)(R r) {
 auto l = r.length;
 return CircularBuffer!R(l-1,r,0,0,makeAdder(l),makeSubber(l),0);
}

unittest {
 import std.algorithm : equal;
 import std.range : only;
 auto cb1 = makeCircularBuffer!(int[])(1);
 cb1.prepend(1);
 auto cb2 = cb1.save;
 cb2.append(2);
 assert(cb1.equal(only(1)));
 assert(cb2.equal(only(2)));
}
unittest {
 import std.algorithm : equal;
 import std.range : only;
 auto cb = makeCircularBuffer!(int[])(7);
 assert(cb.length==0);
 cb.pushBack(1); 
 cb.pushBack(2); 
 cb.pushBack(3); 
 cb.pushBack(4); 
 cb.pushBack(5); 
 assert(cb.length==5);
 cb.pushBack(6); 
 cb.pushBack(7);
 assert(cb.length==7);
 assert(cb.equal(only(1,2,3,4,5,6,7)));
 assert(cb[0]==1);
 cb.pushBack(8); 
 assert(cb.length==7);
 assert(cb.equal(only(2,3,4,5,6,7,8)));
 assert(cb[6]==8);
 cb.pushFront(1);
 cb.pushFront(0);
 assert(cb.equal(only(0,1,2,3,4,5,6)));
 assert(cb[5]==5);
 cb.popFront();
 cb.popBack();
 assert(cb.length==5);
 assert(cb.equal(only(1,2,3,4,5)));
 cb.popFront();
 cb.popBack();
 assert(cb.equal(only(2,3,4)));
 cb.popFront();
 assert(cb.equal(only(3,4)));
 cb.popBack();
 assert(cb.equal(only(3)));
 assert(cb[0]==3);
 cb[0] = 1;
 assert(cb[0]==1);
 cb.popFront();
 assert(cb.empty);
 cb.pushFront(1);
 assert(cb.equal(only(1)));
 cb.popBack();
 assert(cb.empty);
}
unittest {
 import std.algorithm : equal;
 import std.range : only;
 int[8] buf;
 auto cb = makeCircularBuffer(buf[]);
 cb.pushBack(1); 
 cb.pushBack(2); 
 cb.pushBack(3); 
 cb.pushBack(4); 
 cb.pushBack(5); 
 cb.pushBack(6); 
 cb.pushBack(7);
 assert(cb.equal(only(1,2,3,4,5,6,7)));
 assert(cb[1..6].equal(only(2,3,4,5,6)));
 cb.pushBack(8); 
 assert(cb.equal(only(2,3,4,5,6,7,8)));
 cb.pushFront(1);
 cb.pushFront(0);
 assert(cb.equal(only(0,1,2,3,4,5,6)));
 cb.popFront();
 cb.popBack();
 assert(cb.equal(only(1,2,3,4,5)));
 cb.popFront();
 cb.popBack();
 assert(cb.equal(only(2,3,4)));
 cb.popFront();
 assert(cb.equal(only(3,4)));
 cb.popBack();
 assert(cb.equal(only(3)));
 cb.popFront();
 assert(cb.empty);
 cb.pushFront(1);
 assert(cb.equal(only(1)));
 cb.popBack();
 assert(cb.empty);
}
