module containers.automem.list;
import std.range : ElementType, drop;
import std.traits : isInstanceOf;
import std.experimental.allocator.mallocator;
import automem;

alias Node(T) = RefCounted!(Ele!T,Mallocator);
struct Ele(T) {
 T val;
 RefCounted!(Ele!T,Mallocator) next;
}

auto cons(T,N)(T v, N rest) {
 return N(v,rest);
}

auto front(N)(N n) {
 return n.val;
}

auto tail(N)(N n) {
 return n.next;
}

auto empty(N)(N n) {
 return n == null;
} 

auto toListReverse(R)(R r)  {
 alias T = ElementType!R;
 auto ret = Node!T.init;
 while(!r.empty) {
  ret = cons(r.front,ret);
  r.popFront;
 }
 return ret;
}

//rangeWrapper
struct ListRange(T) {
 Node!T n;
 auto empty() { return n.empty; }
 auto front() { return n.front; }
 void popFront() { n = n.next; }
 auto save() { return typeof(this)(n); }
}

auto asRange(N)(N n) {
 return ListRange!(typeof(n.val))(n);
}

auto reverse(N)(N n) {
 return toListReverse(asRange(n));
}

auto toList(R)(R r) {
 return r.toListReverse.reverse;
}

auto makeList(T)() {
 return Node!T.init;
}
auto makeList(T)(T v) {
 return cons(v,makeList!T());
}

void setFront(N,T)(N n, T v) {
 n.val = v;
}
void setTail(N)(N n, N t) {
 n.next = t;
}

unittest {
 import std.algorithm;
 import std.range;
 auto l = cons(1,cons(2,cons(3,Node!int.init)));
 assert(l.front == 1);
 assert(l.next.front == 2);
 assert(l.next.next.front == 3);
 assert(l.next.next.next.empty);
 l = toList(only(3,2,1));
 assert(l.front == 3);
 assert(l.next.front == 2);
 assert(l.next.next.front == 1);
 assert(l.next.next.next.empty);
 l.setFront(1);
 l.next.next.setTail(l); //a loop
 assert(l.front == 1);
 assert(l.next.front == 2);
 assert(l.next.next.front == 1);
 assert(l.next.next.next.front == 1);
 assert(l == l.next.next.next);
 auto list = asRange(toListReverse(only(1,2,3)));
 assert(list.equal(only(3,2,1)));
 l = typeof(l).init;
 assert(l.asRange.empty);
}

