module containers.automem.persistentarray;
import containers.redblacktree;
import std.typecons : tuple, Tuple;
import std.range : take;

struct PersistentArray(T) {
 ulong offset = 0;
 ulong length = 0;
 RedBlackTree!((a,b)=>true, T) rbt;
 auto inRange(ulong i) { return i+offset<length; }
 auto set(ulong i, T v) {
  return typeof(this)(offset,(inRange(i)?length:length+1),rbt.replaceIth(i+offset,v));
 }
 auto opIndex(ulong i) {
  assert(inRange(i));
  return rbt.ithValue(i+offset);
 }
 auto get(ulong i, T d) {
  return (inRange(i)?rbt.ithValue(i+offset):d);
 } 
 auto insertBefore(ulong i, T v) {
  return typeof(this)(offset, length+1,rbt.insertBeforeIth(i+offset,v));
 }
 auto insertAfter(ulong i, T v) {
  return insertBefore(i+1,v);
 }
 auto pushFront(T v) {
  return insertBefore(0,v);
 }
 auto pushBack(T v) {
  return insertBefore(length,v);
 }
 auto opIndex(ulong i, ulong j) {
  assert(i<=j && j <= length);
  return typeof(this)(offset+i,j-i,rbt);
 }
 auto asRange() {
  auto p = rbt.pathToIth(offset);
  return RedBlackTreeRange!(typeof(p))(p).take(length);
 }
}
unittest{
 import std;
 auto v = PersistentArray!int.init;
 foreach(i; 0..5) { v = v.set(i,i); }
 assert(v.length == 5);
 assert(v.rbt.redBlackTreeAsRange.equal(only(0,1,2,3,4)));
 v = v.set(100,5);
 assert(v.rbt.redBlackTreeAsRange.equal(only(0,1,2,3,4,5)));
 v = v.pushFront(-1);
 v = v.pushFront(-2);
 v = v.insertBefore(3,9);
 assert(v.rbt.redBlackTreeAsRange.equal(only(-2,-1,0,9,1,2,3,4,5)));
 auto w = v.pushBack(9);
 v = v.set(0,17);
 assert(v.get(0,23)==17);
 assert(w.rbt.redBlackTreeAsRange.equal(only(-2,-1,0,9,1,2,3,4,5,9)));
 assert(v.rbt.redBlackTreeAsRange.equal(only(17,-1,0,9,1,2,3,4,5)));
 assert(v.get(1000,12345)==12345);
 assert(v[0]==17);
 assert(v[1]==-1);
 auto x = v[4,7];
 assert(x.asRange.equal(only(1,2,3)));
 x = x.insertBefore(1,1);
 assert(x.asRange.equal(only(1,1,2,3)));
 assert(v.asRange.equal(only(17,-1,0,9,1,2,3,4,5)));
}

