module containers.persistentmap;
import std.algorithm : map;
import containers.redblacktree;
import containers.list;
import std.typecons;
struct Map(alias less,K,V) {
 RedBlackTree!((a,b)=>less(a[0],b[0]),Tuple!(K,V)) rbt;
 auto isAssigned(K k) {
  auto r = rbt.pathToInsertValue(tuple(k,V.init)).asRange;
  return !r.empty && !r.front.node.isLeaf && r.front.node.value[0] == k;
 }
 auto set(K k, V v) {
  return typeof(this)(rbt.insert(tuple(k,v)));
 }
 auto get(K k,V d=V.init) {
  auto n = rbt.pathToInsertValue(tuple(k,d)).front.node;
  return (n.isLeaf?d:n.value[1]);
 }
 auto remove(K k) {
  return typeof(this)(rbt.remove(tuple(k,V.init)));
 }
 auto asRange() {
  return rbt.redBlackTreeAsRange;
 }
}
auto makeMap(alias less=((a,b)=>a<b),K,V)() {
 return Map!(less,K,B).init;
}
unittest {
 import std;
 auto m = Map!((a,b)=>a<b,int,string).init;
 m = m.set(1,"dog");
 assert(m.get(1,"cow")=="dog");
 m = m.set(1,"cow");
 m = m.set(2,"ant");
 m = m.set(3,"bug");
 assert(m.get(1,"dog")=="cow");
 m = m.remove(1);
 assert(m.get(1,"pig")=="pig");
 assert(m.isAssigned(2));
 assert(!m.isAssigned(1));
}
