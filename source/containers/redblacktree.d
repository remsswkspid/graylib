module containers.redblacktree;
import std.stdio;
import std.format;
import std.range;
import containers.list;
import std.traits;
import std.algorithm;
struct RedBlackNode(T) {
 bool _red;
 RedBlackNode!T* left;
 RedBlackNode!T* right;
 T value;
 ulong cnt;
 auto updateRed(bool r) {
  return new typeof(this)(r,left,right,value,cnt);
 }
 auto updateLeft(RedBlackNode!T* l) {
  return makeRedBlackNode!(typeof(this),T)(_red,l,right,value);
 }
 auto updateRight(RedBlackNode!T* r) {
  return makeRedBlackNode!(typeof(this),T)(_red,left,r,value);
 }
 auto updateValue(T v) {
  return new typeof(this)(_red,left,right,v,cnt);
 }
 auto uncle(bool isLeft) { //change name to other sibling
  return (isLeft?right:left);
 }
 auto sibling(bool isLeft) { return uncle(isLeft); }
}
auto min(N)(N* n) {
 assert(!n.isLeaf);
 auto curMin = n.value;
 n = n.left;
 while(!n.isLeaf) {
  curMin = n.value;
  n = n.left;  
 }
 return curMin;
}

auto max(N)(N* n) {
 assert(!n.isLeaf);
 auto curMax = n.value;
 n = n.right;
 while(!n.isLeaf) {
  curMax = n.value;
  n = n.right;  
 }
 return curMax;
}


auto red(RBN)(RBN* n) {
 return !n.isLeaf && n._red;
}
auto black(RBN)(RBN* n) {
 return !n.red;
}
auto update(RBN)(RBN* n, RBN* x, bool left) {
 return (left?n.updateLeft(x):n.updateRight(x));
}

auto toString(RBN)(RBN* n)
 if (isInstanceOf!(RedBlackNode, RBN)) {
 return (n is null? "[]":format("[%s,%s,%s]",n.red?"red":"black",n.value,n.cnt));
}

auto isLeaf(N)(N* n) { return (n == null); }

auto isExternal(N)(N* n) { return n.isLeaf || n.left.isLeaf || n.right.isLeaf; }

auto isInner(N)(N* n) { return !n.isExternal; }

auto count(RBN)(RBN* n) { return (n.isLeaf? 0:n.cnt); }

struct CheckAndEvaluate {
 bool good;
 ulong count;
 ulong maxBlackHeight;
}
auto checkAndEvaluate(RBN)(RBN* n) {
 if(n.isLeaf){ return CheckAndEvaluate(true,0,1); }
 bool good = (!n.red || (!n.left.red && !n.right.red));
 auto l = checkAndEvaluate(n.left);
 auto r = checkAndEvaluate(n.right);
 good = good && (l.maxBlackHeight == r.maxBlackHeight);
 auto count = l.count + r.count + 1;
 return CheckAndEvaluate(l.good&&r.good&&count==n.count,count,l.maxBlackHeight + (n.red?0:1));
}

auto check(RBN)(RBN* n) {
 return n.checkAndEvaluate.good; 
} 

auto makeRedBlackNode(RBN, T)(bool red, RBN* l, RBN* r, T v) {
 return new RBN(red,l,r,v,1+count(r)+count(l));
}

auto makeRedBlackEndNode(T)(bool red, T v) {
 alias RBN = RedBlackNode!T;
 return makeRedBlackNode!(RBN,T)(red,cast(RBN*) null, cast(RBN*) null, v);
}

auto rotateLeft(RBN)(RBN* n) {
 auto r = n.right;
 return r.updateLeft(n.updateRight(r.left));
}

auto rotateRight(RBN)(RBN* n) {
 auto l = n.left;
 return l.updateRight(n.updateLeft(l.right));
}

auto swapColor(RBN)(RBN* n) {
 return n.updateRed(!n.red);
}

auto insertFixLeftLeft(RBN)(RBN* g) {
 auto n = g.rotateRight;
 return makeRedBlackNode(false, n.left, n.right.swapColor, n.value);
}

auto insertFixLeftRight(RBN)(RBN* g) {
 return makeRedBlackNode(g.red,g.left.rotateLeft,g.right,g.value).insertFixLeftLeft;
}

auto insertFixRightRight(RBN)(RBN* g) {
 auto n = g.rotateLeft;
 return makeRedBlackNode(false,n.left.swapColor, n.right, n.value);
}

auto insertFixRightLeft(RBN)(RBN* g) {
 return makeRedBlackNode(g.red,g.left,g.right.rotateRight,g.value).insertFixRightRight;
}

auto insertFix(RBN)(RBN* g, RBN* p, RBN* u, RBN* x, bool pLeft, bool xLeft) {
 if (!p.red || !x.red) { return g; }
 if (!u.isLeaf() && u.red) { return makeRedBlackNode(true, g.left.swapColor, g.right.swapColor,g.value); }
 if (pLeft && xLeft) { return g.insertFixLeftLeft; }
 if (pLeft) { return g.insertFixLeftRight; }
 if (xLeft) { return g.insertFixRightLeft; }
 return g.insertFixRightRight;
}

auto removeFixLeftLeft(RBN)(RBN* p) {
 auto f(RBN* n, bool red, RBN* l) {
  return n.updateLeft(l).updateRed(red);
 }
 return f(p,false,f(p.left,p.red,p.left.left.updateRed(false))).rotateRight;
}

auto removeFixLeftRight(RBN)(RBN* p) {
 auto n = p.left.rotateLeft;
 return p.updateLeft(n.updateLeft(n.left.updateRed(true)).updateRed(false)).removeFixLeftLeft;
}

auto removeFixRightRight(RBN)(RBN* p) {
 auto f(RBN* n, bool red, RBN* r) {
  return n.updateRight(r).updateRed(red);
 }
 return f(p,false,f(p.right,p.red,p.right.right.updateRed(false))).rotateLeft;
}

auto removeFixRightLeft(RBN)(RBN* p) {
 auto n = p.right.rotateRight;
 return p.updateRight(n.updateRight(n.right.updateRed(true)).updateRed(false)).removeFixRightRight;
}

auto removeFixLeft(RBN)(RBN* p) {
 return p.updateLeft(p.left.updateRed(false)).updateRed(true).rotateRight;
}

auto removeFixRight(RBN)(RBN* p) {
 return p.updateRight(p.right.updateRed(false)).updateRed(true).rotateLeft;
}

auto swapContNext(RBN,T,P)(RBN* n, T v, P path) {
 alias PE = typeof(path.front);
 return cons(PE(n.updateValue(v),path.front.side),path.tail).continuePathToNextInsertLocation.tail;
}


enum Side {left, right, neither}

struct PathEle(T) {
 RedBlackNode!T* node;
 Side side;
 auto toString() { return format!"<%s,%s>"(node.toString(),side); }
}


auto makeRedBlackNodeBlack(RBN)(RBN* n) {
 if (n.isLeaf) { return n; }
 return n.updateRed(false);
}

void displayNode(RBTN)(RBTN* root) {
  void f(typeof(root) n,int level,string last,bool[] rights) {
   writefln("%-(%s%)%s%s",rights.retro.drop(1).retro.map!(x=>(x?"|    ":"     ")),last,n.toString);
   if(!n.isLeaf){
    f(n.right,level+1 ,"├── ",rights~true);
    f(n.left,level+1,"└── ",rights~false);
   }
  } 
  f(root,0,"",[]); 
}

struct RedBlackTree(alias less, T) {
 alias PRBN = RedBlackNode!T*;
 alias PE = PathEle!T;
 PRBN root;
 auto size() { return count(root); }

 auto pathToInsertValue(T v) {
  PRBN n = root;
  auto path = makeList(PE(root,Side.neither));
  while(true) {
   if(n.isLeaf) { return path; }
   if(less(v,n.value)) {
    n = n.left;
    path = cons(PE(n,Side.left),path);
   } else if (less(n.value,v)) {
    n = n.right;
    path = cons(PE(n,Side.right),path);
   } else {
    return path;
   }
  }
 }
 auto isElem(T v) {
  auto r = pathToInsertValue(v).asRange.map!(n=>n.node.value);
  return !r.empty && r.front == v;
 }
 void display() {
  displayNode(root);
 }
 auto min() {
  return root.min;
 }
 auto max() {
  return root.max;
 }
 auto valid() {
  return root.check; 
 } 
 auto pathToIth(ulong i) {
  return pathToBeforeIth(i+1);
 }
 auto pathToBeforeIth(ulong i) {
  PRBN n = root;
  auto path = makeList(PE(root,Side.neither));
  ulong leftCountPlus1;
  while(true) {
   if(n.isLeaf) { return path; }
   leftCountPlus1 = n.left.count + 1;
   if(i < leftCountPlus1) {
    n = n.left;
    path = cons(PE(n,Side.left),path);
   } else if (leftCountPlus1 < i) {
    n = n.right;
    path =cons(PE(n,Side.right),path);
    i -= leftCountPlus1;
   } else {
    return path;
   }
  } 
 }
 auto ithValue(ulong i) {
  return pathToIth(i).front.node.value;
 }
 auto pathToInsertBeforeIth(ulong i) {
  return pathToBeforeIth(i).continuePathToNextInsertLocation;
 }
 auto pathToInsertAfterIth(ulong i) {
  return pathToIth(i).continuePathToNextInsertLocation;
 }
 auto insert(P)(P path, T v) {
  if (path.empty) { return this; }
  return typeof(this)(path.insertPath(v).makeRedBlackNodeBlack);
 }
 auto insert(T v) {
  return insert(pathToInsertValue(v),v);
 }
 auto insertBeforeIth(ulong i, T v) {
  return insert(pathToInsertBeforeIth(i),v);
 }
 auto insertAfterIth(ulong i, T v) {
  return insert(pathToInsertAfterIth(i),v);
 }

 auto replaceIth(ulong i, T v) {
  return insert(pathToIth(i),v);
 }
 
 auto insertRange(R)(R r)
 if (is (ElementType!R ==T)) {
  typeof(this) ret;
  foreach(v; r) { ret = ret.insert(v); }
  return ret;
 }
 auto pathToRemove(T v)  {
  auto n = root;
  auto path = makeList(PE(root,Side.neither));
  while(true) {
   if (n.isLeaf) { return makeList!PE; }
   auto w = n.value;
   if (less(v,w)) {
    n = n.left;
    path = cons(PE(n,Side.left),path);
   } else if(less(w,v)) {
    n = n.right;
    path = cons(PE(n,Side.right),path);
   } else if(n.isInner) {
    return n.swapContNext(n.right.min, path);
   } else {
    return path;
   }
  }
 }
 auto pathToRemoveIth(ulong i)  {
  auto n = root;
  auto path = makeList(PE(root,Side.neither));
  i++;
  while(true) {
   if (n.isLeaf) { return makeList!PE; }
   auto lcPlus1 = n.left.count +1;
   if (i < lcPlus1) {
    n = n.left;
    path = cons(PE(n,Side.left),path);
   } else if(lcPlus1 < i) {
    i -= lcPlus1;
    n = n.right;
    path = cons(PE(n,Side.right),path);
   } else if(n.isInner) {
    return n.swapContNext(n.right.min, path);
   } else {
    return path;
   }
  }
 }
 auto rbtRemovePath(P)(P path) {
  if(path.empty) { return this; }
 return typeof(this)(path.removePath);
 }
 auto remove(T v) {
  return rbtRemovePath(pathToRemove(v));
 }
 auto removeIth(ulong i) {
  return rbtRemovePath(pathToRemoveIth(i));
 }
 auto removeRange(R)(R r) {
  typeof(this) ret = this;
  foreach(v; r) { ret = ret.remove(v); }
  return ret;
 }
}
auto continuePathToNextInsertLocation(P)(P path) {
 alias PE = typeof(path.front);
 alias PRBN = typeof(PE.init.node);
 if (path.empty || path.front.node.isLeaf) { return path; }
 PRBN n = path.front.node.right;
 path = cons(PE(n,Side.right),path);
 while(true) {
  if(n.isLeaf) { return path; }
  n = n.left;
  path = cons(PE(n,Side.left),path);
 }
}

auto nextPath(P)(P path) {
 alias PE = typeof(path.front);
 if(path.empty) { return path; }
 if(path.front.node.right.isLeaf) {
  auto side = path.front.side;
  path = path.tail;
  while(!path.empty && side==Side.right) {
   side = path.front.side;
   path = path.tail;
  }
 } else {
  auto n = path.front.node.right;
  path = cons(PE(n,Side.right),path);
  while(!n.left.isLeaf) {
   n = n.left;
   path = cons(PE(n,Side.left),path);
  }
 }
 return path;
}

struct RedBlackTreeRange(P) {
 P p;
 auto front() { return p.front.node.value; }
 auto empty() { return p.empty || p.front.node.isLeaf; }
 void popFront() { p = p.nextPath; }
 auto save() { return typeof(this)(p); }
}

auto redBlackTreeAsRange(RBT)(RBT t)
if (isInstanceOf!(RedBlackTree,RBT)) {
 auto path = t.pathToIth(0);
 return RedBlackTreeRange!(typeof(path))(path);
}

auto insertPath(P,T)(P path, T v) {
 auto n = path.front.node;
 auto x = (n.isLeaf? makeRedBlackEndNode(true,v):n.updateValue(v));
 auto xLeft = path.front.side==Side.left;
 path = path.tail;
 while(true) {
  if(path.empty) { break; }
  assert(!path.front.node.isLeaf);
  auto p = path.front.node.update(x,xLeft); 
  auto pLeft = path.front.side == Side.left;
  path = path.tail;
  if(path.empty) { x=p; break; }
  assert(!path.front.node.isLeaf);
  auto g = path.front.node.update(p,pLeft);
  auto u = g.uncle(pLeft);
  x = insertFix(g,p,u,x,pLeft,xLeft);
  xLeft = path.front.side == Side.left;
  path = path.tail;
 }
 return x;
}

auto removePath(P)(P path) {
 alias PE = typeof(path.front);
 alias PRBN = typeof(PE.init.node);
 alias T = typeof(PE.init.node.value);
 if(path.empty) { return cast(PRBN) null; }
 auto v = path.front.node;
 PRBN p,u,s,l,r,np;
 bool doubleBlack, uLeft, pLeft;
 auto vLeft = path.front.side==Side.left;
 path = path.tail;
 u = v.left.isLeaf?v.right:v.left;
 doubleBlack = u.black && v.black;
 u = u.makeRedBlackNodeBlack;
 uLeft = vLeft;
 while(true) {
  if (path.empty) { return u; }
  p = path.front.node;
  pLeft = path.front.side == Side.left;
  path = path.tail;
  p = p.update(u,uLeft);
  if (!doubleBlack) {
   u = p;
   uLeft = pLeft;
   continue;  
  } else {
   while(true) {
    s = p.sibling(uLeft);
    if (s.red) {
     if(uLeft) {
      np = p.removeFixRight;
      p = np.left;
      u = p.left;
      path = cons(PE(np,pLeft?Side.left:Side.right),path);
      pLeft = true;
      uLeft = true;
     } else {
      np = p.removeFixLeft;
      p = np.right;
      u = p.right;
      path = cons(PE(np,pLeft?Side.left:Side.right),path);
      pLeft = false;
      uLeft = false;
     }
     continue; 
    } else {
     l = s.left;
     r = s.right;
     if(uLeft) {
      uLeft = pLeft;
      if (l.red) {
       u = p.removeFixRightLeft;
       doubleBlack = false;
      } else if(r.red) {
       u= p.removeFixRightRight;
       doubleBlack = false;
      } else {
       u = makeRedBlackNode(false, u, s.updateRed(true), p.value);
       doubleBlack = p.black;
      }
     } else {
      uLeft = pLeft;
      if (r.red) {
       u = p.removeFixLeftRight;
       doubleBlack = false;
      } else if(l.red) {
       u= p.removeFixLeftLeft;
       doubleBlack = false;
      } else {
       u = makeRedBlackNode(false, s.updateRed(true), u, p.value);
       doubleBlack = p.black;
      }
     }
     break;
    }
   }
  }
 }
}

unittest{
 import std;
 //              a
 //             / \
 //            /   \
 //           /     \
 //          /       \
 //         /         \ 
 //        b           c
 //       / \         / \
 //      /   \       /   \
 //     /     \     /     \
 //    /       \   /       \
 //   d         e *         f
 //  / \       / \          /\
 // /   \     /   \        /  \
 //*     *   g     h      *    *
 //         / \   / \
 //        *   * *   *
 alias RTN = RedBlackNode!int;
 alias PRTN = RTN*;
 auto h = makeRedBlackNode(true,cast(PRTN) null, cast(PRTN) null, 8);
 auto g = makeRedBlackNode(true,cast(PRTN) null, cast(PRTN) null, 5);
 auto f = makeRedBlackNode(true,cast(PRTN) null, cast(PRTN) null, 15);
 auto e = makeRedBlackNode(false,g, h, 7);
 auto d = makeRedBlackNode(false,cast(PRTN) null, cast(PRTN) null, 1);
 auto c = makeRedBlackNode(false,cast(PRTN) null,f, 14);
 auto b = makeRedBlackNode(true,d, e, 2);
 auto a = makeRedBlackNode(false,b, c, 11);
 auto rbt = RedBlackTree!((a,b)=>a<b,int)(a);
 assert(rbt.isElem(2));
 auto p1 = rbt.pathToInsertValue(2);
 auto p2 = p1.continuePathToNextInsertLocation;
 auto p3 = rbt.pathToIth(4);
 auto p4 = rbt.pathToIth(0);
 assert(p1.asRange.map!(n=>n.node.value).equal(only(2,11)));
 assert(p2.asRange.drop(1).map!(n=>n.node.value).equal(only(5,7,2,11)));
 assert(p3.asRange.map!(n=>n.node.value).equal(only(8,7,2,11)));
 assert(p4.asRange.map!(n=>n.node.value).equal(only(1,2,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(2,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(5,7,2,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(7,2,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(8,7,2,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(14,11)));
 p4 = p4.nextPath;
 assert(p4.asRange.map!(n=>n.node.value).equal(only(15,14,11)));
}
unittest{
 auto rbt = RedBlackTree!((a,b)=>a<b,int)(null);
 rbt = rbt.insert(8);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 1);
 assert(rbt.root.left.isLeaf);
 assert(rbt.root.right.isLeaf);
 rbt = rbt.insert(18);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 2);
 assert(rbt.root.left.isLeaf);
 assert(rbt.root.right.red);
 assert(rbt.root.right.value == 18);
 assert(rbt.root.right.count == 1);
 assert(rbt.root.right.left.isLeaf);
 assert(rbt.root.right.right.isLeaf);
 rbt = rbt.insert(5);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 3);
 assert(rbt.root.left.red);
 assert(rbt.root.left.value == 5);
 assert(rbt.root.left.count == 1);
 assert(rbt.root.left.left.isLeaf);
 assert(rbt.root.left.right.isLeaf);
 assert(rbt.root.right.red);
 assert(rbt.root.right.value == 18);
 assert(rbt.root.right.count == 1);
 assert(rbt.root.right.left.isLeaf);
 assert(rbt.root.right.right.isLeaf);
 rbt = rbt.insert(15);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 4);
 assert(!rbt.root.left.red);
 assert(rbt.root.left.value == 5);
 assert(rbt.root.left.count == 1);
 assert(rbt.root.left.left.isLeaf);
 assert(rbt.root.left.right.isLeaf);
 assert(!rbt.root.right.red);
 assert(rbt.root.right.value == 18);
 assert(rbt.root.right.count == 2);
 assert(rbt.root.right.left.red);
 assert(rbt.root.right.left.value == 15);
 assert(rbt.root.right.left.count == 1);
 assert(rbt.root.right.left.left.isLeaf);
 assert(rbt.root.right.left.right.isLeaf);
 assert(rbt.root.right.right.isLeaf);
 rbt = rbt.insert(17);
 assert(!rbt.root.isLeaf);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 5);
 assert(!rbt.root.left.isLeaf);
 assert(!rbt.root.left.red);
 assert(rbt.root.left.value == 5);
 assert(rbt.root.left.count == 1);
 assert(rbt.root.left.left.isLeaf);
 assert(rbt.root.left.right.isLeaf);
 assert(!rbt.root.right.isLeaf);
 assert(!rbt.root.right.red);
 assert(rbt.root.right.value == 17);
 assert(rbt.root.right.count == 3);
 assert(!rbt.root.right.left.isLeaf);
 assert(rbt.root.right.left.red);
 assert(rbt.root.right.left.value == 15);
 assert(rbt.root.right.left.count == 1);
 assert(rbt.root.right.left.left.isLeaf);
 assert(rbt.root.right.left.right.isLeaf);
 assert(!rbt.root.right.right.isLeaf);
 assert(rbt.root.right.right.red);
 assert(rbt.root.right.right.value == 18);
 assert(rbt.root.right.right.count == 1);
 assert(rbt.root.right.right.left.isLeaf);
 assert(rbt.root.right.right.right.isLeaf);
 rbt = rbt.insert(25);
 assert(!rbt.root.isLeaf);
 assert(!rbt.root.red);
 assert(rbt.root.value == 8);
 assert(rbt.root.count == 6);
 assert(!rbt.root.left.isLeaf);
 assert(!rbt.root.left.red);
 assert(rbt.root.left.value == 5);
 assert(rbt.root.left.count == 1);
 assert(rbt.root.left.left.isLeaf);
 assert(rbt.root.left.right.isLeaf);
 assert(!rbt.root.right.isLeaf);
 assert(rbt.root.right.red);
 assert(rbt.root.right.value == 17);
 assert(rbt.root.right.count == 4);
 assert(!rbt.root.right.left.isLeaf);
 assert(!rbt.root.right.left.red);
 assert(rbt.root.right.left.value == 15);
 assert(rbt.root.right.left.count == 1);
 assert(rbt.root.right.left.left.isLeaf);
 assert(rbt.root.right.left.right.isLeaf);
 assert(!rbt.root.right.right.isLeaf);
 assert(!rbt.root.right.right.red);
 assert(rbt.root.right.right.value == 18);
 assert(rbt.root.right.right.count == 2);
 assert(rbt.root.right.right.left.isLeaf);
 assert(!rbt.root.right.right.right.isLeaf);
 assert(rbt.root.right.right.right.value == 25);
 assert(rbt.root.right.right.right.count == 1);
 assert(rbt.root.right.right.right.left.isLeaf);
 assert(rbt.root.right.right.right.right.isLeaf);
 auto rbt2 = RedBlackTree!((a,b)=>a>b,int)(null);
 rbt2 = rbt2.insert(8);
 rbt2 = rbt2.insert(18);
 rbt2 = rbt2.insert(5);
 rbt2 = rbt2.insert(15);
 rbt2 = rbt2.insert(17);
 rbt2 = rbt2.insert(25);
 rbt2 = rbt2.insert(40);
 rbt2 = rbt2.insert(80);
 assert(rbt2.valid);
 auto rbt3 = rbt2;
 rbt3= rbt2.remove(8);
 assert(rbt3.valid);
 rbt3.redBlackTreeAsRange.writeln;
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,25,18,17,15,5)));
 rbt3= rbt2.remove(5);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,25,18,17,15,8)));
 rbt3 = rbt2.remove(18);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,25,17,15,8,5)));
 rbt3 = rbt2.remove(15);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,25,18,17,8,5)));
 rbt3 = rbt2.remove(17);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,25,18,15,8,5)));
 rbt3 = rbt2.remove(25);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,40,18,17,15,8,5)));
 rbt3 = rbt2.remove(40);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(80,25,18,17,15,8,5)));
 rbt3 = rbt2.remove(80);
 assert(rbt3.valid);
 assert(rbt3.redBlackTreeAsRange.equal(only(40,25,18,17,15,8,5)));
}

version(fulltest) {
unittest{
 import std;
 import core.memory;
 auto v = [1,2,3,4,5,6,7];//,8,9,10];
 auto p = v.permutations.map!array.array;
 foreach(ind,s; p) {
  ind.writeln;
  foreach(x; p) {
   auto rbt = RedBlackTree!((a,b)=>a<b,int)(null);
   auto w = s.array;
   foreach(i,a; w) {
    rbt = rbt.insert(a);
    assert(rbt.valid);
    assert(rbt.redBlackTreeAsRange.equal(w[0..(i+1)].dup.sort));
   }
   foreach(j,b; x) {
    rbt = rbt.remove(b);
    assert(rbt.valid);
    assert(rbt.redBlackTreeAsRange.equal(x[(j+1)..$].dup.sort));
   }
  }
 } 
}
}

