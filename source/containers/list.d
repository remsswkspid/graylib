module containers.list;
import std.range : ElementType, drop;
import std.traits : isInstanceOf;

struct Ele(T) {
 T val;
 Ele!T* next = null;
}
alias Node(T) = Ele!T*;

pure auto cons(T,N)(T v, N rest)
if (is(Ele!T* == N)) {
 return new Ele!T(v,rest);
}

pure auto front(N)(N n) {
 return n.val;
}

pure auto tail(N)(N n) {
 return n.next;
}

pure auto empty(N)(N n) {
 return n is null;
} 

pure auto toListReverse(R)(R r)  {
 alias T = ElementType!R;
 auto ret = cast(Node!T) null;
 while(!r.empty) {
  ret = cons(r.front,ret);
  r.popFront;
 }
 return ret;
}

//rangeWrapper
struct ListRange(T) {
 Node!T n;
 auto empty() { return n.empty; }
 auto front() { return n.front; }
 void popFront() { n = n.next; }
 auto save() { return typeof(this)(n); }
}

pure auto asRange(N)(N n) {
 return ListRange!(typeof(n.val))(n);
}

pure auto reverse(N)(N n) {
 return toListReverse(asRange(n));
}

pure auto toList(R)(R r) {
 return r.toListReverse.reverse;
}

pure auto makeList(T)() {
 return cast(Node!T) null;
}
pure auto makeList(T)(T v) {
 return cons(v,makeList!T());
}

void setFront(N,T)(N n, T v) {
 n.val = v;
}
void setTail(N)(N n, N t) {
 n.next = t;
}

unittest {
 import std.algorithm;
 import std.range;
 auto l = cons(1,cons(2,cons(3,cast(Node!int) null)));
 assert(l.front == 1);
 assert(l.next.front == 2);
 assert(l.next.next.front == 3);
 assert(l.next.next.next.empty);
 l = toList(only(3,2,1));
 assert(l.front == 3);
 assert(l.next.front == 2);
 assert(l.next.next.front == 1);
 assert(l.next.next.next.empty);
 l.setFront(1);
 l.next.next.setTail(l); //a loop
 assert(l.front == 1);
 assert(l.next.front == 2);
 assert(l.next.next.front == 1);
 assert(l.next.next.next.front == 1);
 assert(l == l.next.next.next);
 auto list = asRange(toListReverse(only(1,2,3)));
 assert(list.equal(only(3,2,1)));
 l = null;
 assert(l.asRange.empty);
}

