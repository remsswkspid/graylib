module containers.persistenthash;
import std.algorithm : map , joiner;
import containers.persistentmap;
import containers.persistentarray;
import containers.redblacktree;
import containers.list;
import std.typecons;

long less(long a,long b) { return a < b; }

alias PH(K,V) = Map!(less,long,PersistentArray!(Tuple!(K,V))); 
struct PersistentHash(alias hashFunc, K, V) {
  PH!(K,V) store;
  ulong cnt;
  auto set(K k, V v) {
   long l = hashFunc(k);
   auto tmp = store.get(l);
   ulong i = 0;
   foreach(t; tmp.asRange) {
      if(t[0] ==k ) {  return typeof(this)(store.set(l,tmp.set(i,tuple(k,v))),cnt); }
      i++;
   }
   return typeof(this)(store.set(l,tmp.pushBack(tuple(k,v))),cnt+1);
  }
  auto get(K k,V d=V.init) {
    long l = hashFunc(k);
    foreach(t; store.get(l).asRange) {
      if(t[0]==k) { return t[1]; }
    }
    return d;
  }
  auto asRange() {
    return store.asRange.map!(x=>x[1].asRange).joiner;
  }
}
auto persistentHash(alias hashFunc, K,V)() {
  return PersistentHash!(hashFunc,K,V).init;
}
unittest {
  auto test = persistentHash!(x=>x/4,long,string);
  test = test.set(1,"Dog");
  test = test.set(2,"Horse");
  assert(test.get(1) == "Dog"); 
  assert(test.get(2) == "Horse"); 
  auto test2 = test.set(5,"Cow");
  assert(test.get(5,"oops") == "oops"); 
  assert(test2.get(5,"oops") == "Cow"); 

}
