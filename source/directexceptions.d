module directexceptions;
import std.traits;
import std.stdio;
import sumtype;

struct Ok {
}

alias ExceptionOr(T) = SumType!(Exception,T);

alias ExOk = ExceptionOr!Ok;

auto lift(alias f)() {
 alias ET = ExceptionOr!(ReturnType!f);
 try {
  return ET(f());
 } catch (Exception e) {
  return ET(e);
 }
}
//Print out exception and ignore
auto okay(ExOk errOk) {
 errOk.match!(
 (Exception e) { writeln(e); },
 (Ok k) {});
}

//Force user of a function to deal with exceptions
unittest {
 import std.stdio;
 //The function has two independent ways of "returning", namely, normally or by throwing
 auto f(int n) {
  if(n==0) { throw new Exception("Don't dare use zero"); }
  return 17;
 }
 //The function is similar but now returns only the normal way (Either an exception or a int)
 auto fNT(int n) { return lift!(()=>f(n)); }
 //Use f allows one to ignore the possibility of throwing:
 //Following line uncommeted would compile, but throws an
 //un caught exception causeing the program to stop
 //(f(0)+1).writeln);
 //The following doesn't compile
 //(fNT(0)+1).writeln;
 assert(fNT(0).match!(
  (int n)=>(n+1),
  (Exception e) => 59) == 59);
}
