module treedisp;
import std.stdio;
import std.format;
import std.typecons;
import std.traits;

void drawTree(N)(N n, string pref="", ulong maxDepth=1000) {
  static if(__traits(compiles,N.init.dispStr)) {
    writeln(n.dispStr);
  } else {
    writeln(n.defDispStr);
  }
  if(maxDepth == 0) { return; }
  static if(__traits(compiles,N.init.children)) {
    auto ptrChildren = n.children;
  } else {
    auto ptrChildren = n.defChildren;
  }
  if(ptrChildren.length==0) { return; }
  auto children = ptrChildren[0];
  if(children.length > 0) {
    foreach(c; children[0..$-1]) {
      write(pref ~ "├── ");
      drawTree(c, pref ~ "│   ",maxDepth-1);
    }
    write(pref ~ "└── ");
    drawTree(children[$-1], pref ~ "    ",maxDepth-1);
  }
}

auto defDispStr(T)(T t) {
  static if (isBasicType!T || is(T==string)) {
   return format!"%s"(t);
  } else {
   return T.stringof;
  }
}
auto defChildren(T)(T t) {
 static if (is (T U == U*)) {
   if(t==null) { return typeof(defChildren(*t)).init; }
   return defChildren(*t);
 } else static if (is(T U == U[]) && !is(T==string)) {
  return [t];
 } else static if(__traits(compiles,T.init.tupleof)) {
  return [tuple(t.tupleof)];
 } else {
  return (int[][]).init;
 }
}

